# pTASC
**Description:**  Provisioning, Authentication and Confidentiality Layer for Communication.

**Project Name:** Inconnu

**Position on FIWARE architecture:** PEP and IdM, replacing HTTPS on the communication

**Code Documentation Format:** javadoc
****
## Contents
* Architecture
* Compilation
* General Execution Description
* Customizing

### Architecture
pTASC is a highly customizable provisioning, authentication and communication layer enabling communication between devices running the same protocol.

The image bellow depicts an overview of the entire project architecture and the place of pTASC on the architecture 
![Inconnu Architecture](readme_imgs/all.png "Inconnu Architecture")

An overview of the communication to build a smart city infrastructure is depicted of the figure bellow

![Communications Overview](readme_imgs/comms.png "Communication Overview")

For more information read the architecture section of the dissertation

### Compilation
To compile this source it is needed to install the necessary dependencies. The code is built in java 8 and with maven as a dependency and compilation automation tool.

To install java and maven run the following:
```shell
sudo apt-get update
sudo apt-get install openjdk-8-jdk maven
```

Having java and maven installed its required to fetch other inconnu project dependencies that are needed to compile pTASC. To do this run the following
```shell
sudo apt-get install git
git clone https://Iqop20@bitbucket.org/Iqop20/crl.git crl
cd crl 
mvn clean install
```

With the dependencies installed we can now compile the project
```shell
cd ../ptasc
mvn clean package
```
The compilation result is a **jar** file located in: *./target/ptasc-1.2.jar*

### General Execution
The simplest way to execute this code is to run the inconnu.ptasc.bootstrapper samples, to do so run:
```shell
java -cp ./target/ptasc-1.2.jar inconnu.ptasc.bootstrappers.<BOOSTRAPPER NAME>
```
The arguments of each bootstrapper are described on their respective javadocs

These samples provide ways to easily deploy the architecture described above. However, custom clients and managers can be built.

For instructions of how to deploy the smart city infrastructure check the deployment repository of this project (https://bitbucket.org/Iqop20/deployment/)

### Customizing
The current implementation allows to customize the behaviour that the devices have on the architecture by allowing to add custom communication processors and enable/disable services in runtime. For custom processors check **inconnu.ptasc.commmunication.processors.PTASCCommunicationProcessor**.
The best way to customize the behaviour of the devices is to import these code as a maven dependency by doing the following:
```shell
mvn clean install
```
And on your maven based project add the following inside the dependencies tag
```shell
<dependency>
    <groupId>inconnu</groupId>
    <artifactId>ptasc</artifactId>
    <version>1.2</version>
</dependency>
```
****
For any additional information about the inner workings of pTASC, check the **javadoc** documentation