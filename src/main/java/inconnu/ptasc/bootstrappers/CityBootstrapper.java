package inconnu.ptasc.bootstrappers;

import inconnu.crl.Configuration;
import inconnu.ptasc.SystemOutPrintlnMessageHandler;
import inconnu.ptasc.manager.Manager;
import java.io.File;
import java.io.IOException;
import java.security.cert.CertificateEncodingException;

/**
 *  This class initializes the city manager and launches it, making it ready to answer provisioning requests.
 *  <br>
 *  It requires to be passed the following arguments:
 *
 *     <ul>
 *         <li><b>AuthzForce URL</b>: The url where FIWARE's AuthzForce PDP/PAP instance is listening for requests (i.e http://authz:8080/authzforce-ce/)</li>
 *         <li><b>City Domain</b>: The domain of the city, this must be a DNS domain name controlled by you</li>
 *         <li><b>CRL Address</b>: CRL address is the ip address of the CRL server, more information on the dissertation and the README.md</li>
 *         <li><b>YubicoOTP public ID</b>: This is a newly generated yubicoOTP public id, that must be configured on at least one yubikey </li>
 *         <li><b>YubicoOTP secret key</b>: This is the secret key associated with the public id, that must be also configured on the yubikey, more information on the README.md</li>
 *     </ul>
 *
 *     When the CityBootstrapper starts it will generate files containing public and private key information, if the files exist they will not be regenerated, please keep in mind that.
 *     <br>
 *     This process generates a QR code and two files (ECDSAb64.ptasc and ECIESb64.ptasc) that are needed to respectively provision android and cli devices.
 *     <br>
 *     <b>NOTE: </b>In order to be able to receive requests please add the following DNS entry: manager.domain, where domain is the City Domain passed as argument.
 */
public class CityBootstrapper {
    public static void main(String[] args) throws IOException, CertificateEncodingException {
        if(args.length!=5){
            System.out.println("The CityBootstrapper needs the following arguments: authzforceURL cityDomain crlAddress yubikeyPubId yubikeySecret");
            return;
        }

        /*
            Create the necessary files
         */
        File ManagerpubKeyFile = new File("CitypubKey.ptasc");
        File ManagerprivKeyFile = new File("CityprivKey.ptasc");
        File ManagerCertificateFile = new File("CityCertificate.ptasc");
        File ECIESpubKeyFile = new File("CityECIESpubKey.ptasc");
        File ECIESprivKeyFile = new File("CityECIESprivKey.ptasc");
        File QRFile = new File("CityQR.png");
        File ECDSAb64 = new File("ECDSAb64.ptasc");
        File ECIESb64 = new File("ECIESb64.ptasc");
        File yubi = new File("yubi.ptasc");
        yubi.createNewFile();

        /*

         */
        Manager manager = new Manager(args[1],ManagerpubKeyFile,ManagerprivKeyFile,ManagerCertificateFile,ECIESpubKeyFile,ECIESprivKeyFile,QRFile,new SystemOutPrintlnMessageHandler(),ECDSAb64,ECIESb64,args[2],false,yubi);


        //Setting the authzURL enables this manager to create the access policies needed to access FIWARE Orion.
        manager.setAuthzURL(args[0]);

        //Add the yubicoOTP token to be able to complete the provisioning protocol
        manager.addToken(args[3],args[4]);

        //Create the CRL for the city on the CRL server available via the CRLAddress
        inconnu.crl.Client.create(manager.myCertificate[0],args[2], Configuration.SERVER_PORT);
    }
}
