package inconnu.ptasc.bootstrappers;

import inconnu.ptasc.SystemOutPrintlnMessageHandler;
import inconnu.ptasc.client.Client;
import inconnu.ptasc.communication.processors.CRLAddProcessor;
import inconnu.ptasc.communication.processors.CRLVerifyProcessor;
import inconnu.ptasc.communication.processors.GetCertificateProcessor;
import inconnu.ptasc.communication.processors.OrionProcessorPTASC;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import java.io.File;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;


/**
 *  This class initializes and launches an Endpoint Device on the smart city, being its job to forward requests to the smart city internal services,
 *  such as FIWARE Orion, in a confidential, authentic and integral manner. <br>
 *  This is also responsible to perform access control verifications near an AuthzForce server.
 *  <br>
 *  The Endpoint Bootstrapper requires the following arguments to be passed:
 *
 *<ul>
 *     <li><b>City ECIES public key in base64</b>: The ECIES public key associated with the city manager, this can be obtained in the ECIESb64.ptasc file produced by the CityBootstrapper</li>
 *     <li><b>City ECDSA public key in base64</b>: The ECDSA public key associated with the city manager, this can be obtained in the ECDSAb64.ptasc file produced by the CityBootstrapper</li>
 *     <li><b>Yubikey OTP</b>: One OTP produced by the yubikey registered on the City Bootstrapper</li>
 *     <li><b>City Domain</b>: The domain of the city, it will try to reach manager.domain - where domain is the value of this input -, if it fails the endpoint device cannot be provisioned</li>
 *     <li><b>FIWARE Orion URL</b>: The url where FIWARE Orion context broker is listening for requests (i.e. http://localhost:1026/)</li>
 *     <li><b>AuthzForce URL</b>: The url where FIWARE's AuthzForce PDP/PAP instance is listening for requests (i.e http://authz:8080/authzforce-ce/)</li>
 *     <li><b>CRL Address</b>: The address where the CRL server is listening</li>
 *
 *</ul>
 * When the EndpointBootstrapper starts it will generate files containing public and private key information, if the files exist they will not be regenerated and the provisioning process is skipped, please keep in mind that.<br>
 * <b>NOTE: </b> In order to receive requests create a DNS entry called endpoint.domain - where domain is the City Domain passed as input -, if the DNS entry does not exist the communication fails.
 */
public class EndpointBootstrapper {
    public static void main(String[] args) throws Exception {

        if(args.length!=7){
            System.out.println("The EndpointBootstrapper needs the following arguments: CityECIESpubKey CityECDSApubKey  yubikeyCityOTP CityDomain fiwareOrionURL authzforceURL crlAddress");
            return;
        }


        String b64ECIESpubKey = args[0];
        ECPublicKeyParameters ECIESpubKey = (ECPublicKeyParameters) PublicKeyFactory.createKey(java.util.Base64.getDecoder().decode(b64ECIESpubKey));
        String b64ECDSApubKey = args[1];
        PublicKey ECDSApubKey = KeyFactory.getInstance("EC").generatePublic(new X509EncodedKeySpec(java.util.Base64.getDecoder().decode(b64ECDSApubKey)));
        String otp = args[2];
        String cityDomain = args[3];
        String orionURL = args[4];

        /*
            Initialize a client instance and the necessary files
         */
        File certificateFile = new File("EndpointCertificate.ptasc");
        File certificatePublicKeyFile = new File("EndpointCertificatePublicKey.ptasc");
        File certificatePrivKeyFile = new File("EndpointCertificatePrivKey.ptasc");


        Client client = new Client("endpoint", ECIESpubKey, ECDSApubKey, cityDomain, "endpoint."+cityDomain,otp,certificateFile,certificatePublicKeyFile,certificatePrivKeyFile,new SystemOutPrintlnMessageHandler(),args[6],false);

        //Add AuthzForce server url in order to be able to check policies, otherwise all access is denied
        client.setAuthzURL(args[5]);

        /*
            Open Communication service to receive communication and add the necessary processors
            This is highly customizable and new processors can be added to perform different tasks, check the README.md for more information
            Keep in mind that the processor sit after the authentication process, if authorization is also required check
            inconnu.processors.utils.OrionForwardingHandler authorization requests for inspiration
         */
        client.openCommunicationServer();
        client.addCommunicationProcessor(new OrionProcessorPTASC(orionURL));
        client.addCommunicationProcessor(new CRLAddProcessor());
        client.addCommunicationProcessor(new CRLVerifyProcessor());
        client.addCommunicationProcessor(new GetCertificateProcessor());
    }
}
