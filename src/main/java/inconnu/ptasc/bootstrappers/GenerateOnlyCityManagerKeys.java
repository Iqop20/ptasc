package inconnu.ptasc.bootstrappers;

import inconnu.ptasc.PTascNode;
import inconnu.ptasc.SystemOutPrintlnMessageHandler;
import inconnu.ptasc.Utils;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.util.SubjectPublicKeyInfoFactory;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.Objects;

/**This class in a helper class for generating the city manager key files only requiring as input the city domain.<br>
 * The execution of this class outputs the following files: CitypubKey.ptasc, CityprivKey.ptasc, CityCertificate.ptasc, CityECIESpubKey.ptasc, CityECIESprivKey.ptasc, CityQR.png, ECDSAb64.ptasc and ECIESb64.ptasc
 */
public class GenerateOnlyCityManagerKeys {
    public static void main(String[] args) throws IOException {
        if (args.length!=1){
            System.out.println("The city domain name is needed");
            System.exit(-1);
        }

        String managedPoolName = args[0];


        PTascNode pTascNode = new PTascNode(new SystemOutPrintlnMessageHandler());
        File ManagerpubKeyFile = new File("CitypubKey.ptasc");
        File ManagerprivKeyFile = new File("CityprivKey.ptasc");
        File ManagerCertificateFile = new File("CityCertificate.ptasc");
        File ECIESpubKeyFile = new File("CityECIESpubKey.ptasc");
        File ECIESprivKeyFile = new File("CityECIESprivKey.ptasc");
        File QRFile = new File("CityQR.png");

                /*
            Generate public and private key pair (Manager keys)
            Signing
            EC 384
         */
        KeyPair keyPair = Utils.generateECDSAKeyPair(pTascNode,384);
        PublicKey ECDSApubKey;
        PrivateKey ECDSAprivKey;
        if (keyPair != null) {
            ECDSApubKey = keyPair.getPublic();
            ECDSAprivKey = keyPair.getPrivate();

            if ((ManagerpubKeyFile.createNewFile())) {
                System.out.println("File Already Exists");
            } else {
                System.out.println("Created File");
            }

            if ((ManagerprivKeyFile.createNewFile())) {
                System.out.println("File Already Exists");
            } else {
                System.out.println("Created File");
            }

            FileOutputStream fisPub = new FileOutputStream(ManagerpubKeyFile);
            FileOutputStream fisPriv = new FileOutputStream(ManagerprivKeyFile);
            fisPriv.write(ECDSAprivKey.getEncoded());
            fisPub.write(ECDSApubKey.getEncoded());
            fisPriv.close();
            fisPub.close();

        } else return;

        //Manager Self Signed Certificate
        X509Certificate[] myCertificate = new X509Certificate[1];
        myCertificate[0] = Utils.signCSR(pTascNode, Objects.requireNonNull(Utils.generateCSR(pTascNode,ECDSApubKey,ECDSAprivKey, "manager." + managedPoolName)),ECDSAprivKey,new BigInteger("1"),"manager."+managedPoolName,null,365*24*60*60,null);


        if ((ManagerCertificateFile.createNewFile())) {
            System.out.println("File Already Exists");
        } else {
            System.out.println("Created File");
        }
        FileOutputStream fos = new FileOutputStream(ManagerCertificateFile);
        String s = Utils.convertX509CertificateToPEM(pTascNode, myCertificate[0]);
        assert s!=null;
        fos.write(s.getBytes());
        fos.flush();
        fos.close();


        /*
            Generate public and private key pair (ECIES)
            Encryption
            EC 384
         */
        AsymmetricCipherKeyPair keys = Utils.generateECIESKeyPair();
        ECPrivateKeyParameters ECIESprivKey = (ECPrivateKeyParameters) keys.getPrivate();
        ECPublicKeyParameters ECIESpubKey = (ECPublicKeyParameters) keys.getPublic();

        if ((ECIESprivKeyFile.createNewFile())) {
            System.out.println("File Already Exists");
        } else {
            System.out.println("Created File");
        }

        if ((ECIESpubKeyFile.createNewFile())) {
            System.out.println("File Already Exists");
        } else {
            System.out.println("Created File");
        }
        FileOutputStream fisPub = new FileOutputStream(ECIESpubKeyFile);
        FileOutputStream fisPriv = new FileOutputStream(ECIESprivKeyFile);

        fisPub.write(SubjectPublicKeyInfoFactory.createSubjectPublicKeyInfo(ECIESpubKey).getEncoded());
        fisPriv.write(ECIESprivKey.getD().toByteArray());

        fisPub.close();
        fisPriv.close();


        System.out.println("Key generation successful");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("CAECDSA",java.util.Base64.getEncoder().encodeToString(ECDSApubKey.getEncoded()));
        jsonObject.put("CAECIES",java.util.Base64.getEncoder().encodeToString(SubjectPublicKeyInfoFactory.createSubjectPublicKeyInfo(ECIESpubKey).getEncoded()));

        Utils.writeQrToFile(pTascNode,jsonObject.toString(),QRFile);
        new FileOutputStream("ECDSAb64.ptasc").write(java.util.Base64.getEncoder().encode(ECDSApubKey.getEncoded()));
        new FileOutputStream("ECIESb64.ptasc").write(java.util.Base64.getEncoder().encode(SubjectPublicKeyInfoFactory.createSubjectPublicKeyInfo(ECIESpubKey).getEncoded()));


    }
}
