package inconnu.ptasc.bootstrappers;

import inconnu.ptasc.Utils;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.util.SubjectPublicKeyInfoFactory;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;

/**This class is a helper class for the deployment process, being only responsible to generate the public and private key files for the endpoint device,
 * this way removing the need of having to use the yubicoOTP on the provisioning process of each replica of the endpoint device.<br>
 *In order to do its job this helper class requires the following input:
 * <ul>
 *     <li><b>City Domain: </b> The domain of the city where a manager exists</li>
 *     <li><b>City Manager ECDSA private key in base64: </b> The ECDSA private key of the manager of the city domain</li>
 *     <li><b>City Manager certificate in base64: </b> The Cerficicate of the manager of the city domain</li>
 * </ul>
 * The files generated are: EndpointCertificate.ptasc, EndpointCertificatePublicKey.ptasc, EndpointCertificatePrivKey.ptasc
 * <b>NOTE: </b>This is only used to generate the docker image for the endpoint device and to ease its replication, this is not used outside that context since there is no knowledge of the city manager ECDSA private key.
 */
public class GenerateEndpointKeys {

    public static void main(String[] args) throws Exception {

        if (args.length!=3){
            System.out.println("Needs cityDomain b64CityECDSAPriv b64CityManagerCertificate");
        }

        String cityDomain=args[0];
        String b64ECDSACityPriv = args[1];
        String b64CityManagerCertificate = args[2];

        File certificateFile = new File("EndpointCertificate.ptasc");
        File certificatePublicKeyFile = new File("EndpointCertificatePublicKey.ptasc");
        File certificatePrivKeyFile = new File("EndpointCertificatePrivKey.ptasc");


        KeyPair keyPair = Utils.generateECDSAKeyPair(null,384);
        PublicKey ECDSApubKey;
        PrivateKey ECDSAprivKey;
        if (keyPair != null) {
            if ((certificatePublicKeyFile.createNewFile())) {
                System.out.println("File Already Exists");
            } else {
                System.out.println("Created File");
            }
            if ((certificatePrivKeyFile.createNewFile())) {
                System.out.println("File Already Exists");
            } else {
                System.out.println("Created File");
            }
            FileOutputStream cpub = new FileOutputStream(certificatePublicKeyFile);
            FileOutputStream cpriv = new FileOutputStream(certificatePrivKeyFile);
            ECDSAprivKey = keyPair.getPrivate();
            ECDSApubKey = keyPair.getPublic();

            cpub.write(ECDSApubKey.getEncoded());
            cpub.close();
            cpriv.write(ECDSAprivKey.getEncoded());
            cpriv.close();

        } else{
            throw new Exception("ECDSA Key Generation failed");
        }

        byte[] salt = new byte[32];
        new SecureRandom().nextBytes(salt);

        PKCS10CertificationRequest pkcs10CertificationRequest = Utils.generateCSR(null, ECDSApubKey, ECDSAprivKey, "endpoint." + cityDomain);

        assert pkcs10CertificationRequest!=null;

        PrivateKey CityECDSAprivKey = KeyFactory.getInstance("EC").generatePrivate(new PKCS8EncodedKeySpec(Base64.getDecoder().decode(b64ECDSACityPriv)));
        X509Certificate CityCertificate = (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(Base64.getDecoder().decode(b64CityManagerCertificate)));


        X509Certificate certificate = Utils.signCSR(null,pkcs10CertificationRequest, CityECDSAprivKey, new BigInteger(32,new SecureRandom()), "manager."+cityDomain,CityCertificate,365 * 24 * 60 * 60,null);

        //Write cert

        String s = Utils.convertX509CertificateToPEM(null, certificate);
        String s2 = Utils.convertX509CertificateToPEM(null,CityCertificate);

        assert s!=null;
        assert s2!=null;

        FileWriter fs = new FileWriter(certificateFile);
        fs.write(s);
        fs.write(s2);
        fs.flush();
        fs.close();


    }
}
