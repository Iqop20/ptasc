package inconnu.ptasc.client;
import com.google.common.io.ByteStreams;
import inconnu.ptasc.*;
import inconnu.ptasc.communication.Communication;
import inconnu.ptasc.communication.processors.UpdateProcessorPTASC;
import io.grpc.Server;
import io.grpc.netty.shaded.io.grpc.netty.NettyServerBuilder;
import org.apache.commons.lang3.tuple.Pair;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.*;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * The Client PTascNode represents the devices on the architecture that are not managers, including endpoint devices and IoT devices
 */
public class Client extends PTascNode {
    ECPublicKeyParameters CAECIESpubKey;
    PublicKey CApubKey;
    Semaphore updateSemaphore;
    boolean allowUpdates;
    File certificateFile;
    File ECDSAPublicKeyFile;
    File ECDSAPrivKeyFile;

    /**
     * Basic instantiation for manual initialization
     */
    public Client(){
        super(new SystemOutPrintlnMessageHandler());
    }

    /** Complete initialization of the Client, including automatic provisioning into the pool and key file generation
     * @param name The name of the device to be generated
     * @param ECIEScaPubKey The ECIES public key of the manager of the pool
     * @param ECDSAcaPubKey The ECDSA public key of the manager of the pool
     * @param pool The pool where the client device wishes to join, the ECIEScaPubKey and ECIEScaPubKey must match the device in listening on the manager.pool DNS entry
     * @param endpointUSSN The DNS entry where a city endpoint device is listening
     * @param otp The OTP code obtained from the yubikey registered on the pool manager
     * @param certificateFile The file where the device certificate containing the ECDSA public key will be stored
     * @param ECDSAPublicKeyFile The file where the device ECDSA public key will be stored
     * @param ECDSAPrivKeyFile The file where the device ECDSA private key will be stored
     * @param handler The handler provides a customizable way to print messages or errors, the default is the SystemOutPrintlnMessageHandler
     * @param crlIp The crlIP is the address of the crl server, this is only used to verify the revoke of certificates on the communication
     * @param needsToContactExternalCRL On the deployment the CRL server is in the Endpoint device localhost, but is external for any other client. This flag sets the need to contact the external CRL via the endpoint device - endpointUSSN -.
     * @throws Exception If some error on the launching of the client happens an exception is thrown
     */
    public Client(String name,ECPublicKeyParameters ECIEScaPubKey,PublicKey ECDSAcaPubKey,String pool,String endpointUSSN,String otp,File certificateFile,File ECDSAPublicKeyFile,File ECDSAPrivKeyFile,MessageHandler handler,String crlIp,boolean needsToContactExternalCRL) throws Exception {
        super(handler);
        this.name =name;
        this.pool=pool;
        this.crlIp = crlIp;
        this.needsToContactExternalCRL = needsToContactExternalCRL;
        this.CAECIESpubKey = ECIEScaPubKey;
        this.CApubKey = ECDSAcaPubKey;
        this.endpointUSSN = endpointUSSN;
        this.certificateFile = certificateFile;
        this.ECDSAPrivKeyFile = ECDSAPrivKeyFile;
        this.ECDSAPublicKeyFile = ECDSAPublicKeyFile;


        //Do not request updates to the pool manager is the default behaviour, however the setAllowUpdates(boolean) function allows to change this behaviour
        allowUpdates=false;
        updateSemaphore=new Semaphore(1);


        /*
            If some key files do not exist delete all the previous existing ones and write new files with new content
         */
        if (!certificateFile.exists()  || !ECDSAPublicKeyFile.exists() || !ECDSAPrivKeyFile.exists()) {

            certificateFile.delete();
            ECDSAPublicKeyFile.delete();
            ECDSAPrivKeyFile.delete();

            /*
                Generate the ECDSA key pair and store it on the respective files
             */
            KeyPair keyPair = Utils.generateECDSAKeyPair(this,384);
            if (keyPair != null) {
                ECDSAPublicKeyFile.createNewFile();
                ECDSAPrivKeyFile.createNewFile();
                FileOutputStream cpub = new FileOutputStream(ECDSAPublicKeyFile);
                FileOutputStream cpriv = new FileOutputStream(ECDSAPrivKeyFile);
                ECDSAprivKey = keyPair.getPrivate();
                ECDSApubKey = keyPair.getPublic();

                cpub.write(ECDSApubKey.getEncoded());
                cpub.close();
                cpriv.write(ECDSAprivKey.getEncoded());
                cpriv.close();
            } else{
                messageHandler.error(null,"ECDSA Key Generation failed");
                throw new Exception("ECDSA Key Generation failed");
            }



            /*
                Provision the client and store the certificate on the file
                Populate the trustAnchors and certificate information with the information received
             */
            Pair<X509Certificate, List<X509Certificate>> certificateChain = this.provision(CAECIESpubKey,ECDSAcaPubKey,name+"."+pool,pool,otp,certificateFile);


            myCertificate = new X509Certificate[certificateChain.getRight().size() + 1];
            myCertificate[0] = certificateChain.getLeft();
            this.trustAnchors = certificateChain.getRight();

            /*
                The client by default trusts the certification chain of its manager
             */
            for (int i = 0; i < this.trustAnchors.size(); i++) {
                myCertificate[i + 1] = this.trustAnchors.get(i);
            }

            /*
                If provisioning fails throw an exception and delete the files generated
             */
            if (myCertificate[0]==null){
                messageHandler.error(null,"Provisioning Failed");
                messageHandler.error(null,"Deleting all key files");

                ECDSAPrivKeyFile.delete();
                ECDSAPublicKeyFile.delete();
                messageHandler.error(null,"Provisioning Failed");
                throw new Exception("Provisioning failed");
            }


        }else{
            /*
                If the files exist read them and populate the fields
             */
            FileInputStream fis = new FileInputStream(certificateFile);
            CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
            X509Certificate[] certificates = certFactory.generateCertificates(fis).toArray(new X509Certificate[0]);

            trustAnchors = new LinkedList<>();
            X509Certificate myCert=null;
            for(X509Certificate certificate : certificates){
                String subjectName = certificate.getSubjectX500Principal().getName();
                if (subjectName.equals("CN="+name + "." + pool)){
                    myCert = certificate;
                }else{
                    trustAnchors.add(certificate);
                }
            }
            myCertificate = new X509Certificate[trustAnchors.size()+1];
            myCertificate[0] = myCert;
            assert myCert != null;
            this.name = myCert.getSubjectX500Principal().getName().split("CN=")[1].split("\\.")[0];
            this.pool = myCert.getSubjectX500Principal().getName().split("CN=")[1].split("\\.")[1];

            messageHandler.message("Previous Node USSN gathered from certificate: "+this.name+"."+this.pool);

            for (int i=0;i< this.trustAnchors.size();i++){
                myCertificate[i+1] = this.trustAnchors.get(i);
            }

            fis.close();

            FileInputStream cpub = new FileInputStream(ECDSAPublicKeyFile);
            FileInputStream cpriv = new FileInputStream(ECDSAPrivKeyFile);

            byte[] pubKey = ByteStreams.toByteArray(cpub);
            byte[] privKey = ByteStreams.toByteArray(cpriv);


            cpub.close();
            cpriv.close();

            /*
                If the data stored is corrupted delete all the files and exit throwing an error
             */
            try {

                ECDSApubKey = KeyFactory.getInstance("EC").generatePublic(new X509EncodedKeySpec(pubKey));
                ECDSAprivKey = KeyFactory.getInstance("EC").generatePrivate(new PKCS8EncodedKeySpec(privKey));
                messageHandler.message("Keys read successfully from files");


            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                certificateFile.delete();
                ECDSAPrivKeyFile.delete();
                ECDSAPublicKeyFile.delete();
                messageHandler.error(e,"Failed to read keys from files");
                messageHandler.error(e,"Deleting all the client keys");
                throw e;
            }


            /*
                If the certificate signature verification using the manager ECDSA public key fails means that the certificate is corrupted and a deletion of all the key files is issued
                An error is thrown to notify the user.
             */
            try {
                myCertificate[0].verify(ECDSAcaPubKey);
                messageHandler.message("Signature Verified");
            } catch (NoSuchAlgorithmException | InvalidKeyException | NoSuchProviderException | SignatureException e) {
                messageHandler.error(e,"Signature verification failed of the certificate\nDeleting the clientCertificate.ptasc\nTry launching the client again");
                certificateFile.delete();
                ECDSAPrivKeyFile.delete();
                ECDSAPublicKeyFile.delete();
                throw e;
            }


        }

        /*
            Launch a periodic updater that only executes if the allowUpdates flag is set to true
         */
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        scheduledFuture = executorService.scheduleAtFixedRate(() -> {
            if (allowUpdates) {
                if (updateSemaphore.availablePermits() == 1) {
                    periodicUpdater(this);
                }
            }
        }, 0, 30, TimeUnit.SECONDS);
    }

    /**<pre>
     * Allow a client to answer to communication rpcs
     * In addition to this the client must specify which processors to add to the communication pipeline in order to have custom processing, this is done by using the addCommunicationProcessor function
     * </pre>
     */
    public void openCommunicationServer(){
        Thread communication_server = new Thread(() -> {
            Server endpoint=null;
            try {
                endpoint = NettyServerBuilder.forPort(Configuration.DEVICE_COMMUNICATION_PORT)
                        .addService(new Communication.CommunicationService(this))
                        .intercept(new ExceptionHandler())
                        .maxInboundMessageSize(Configuration.MAX_MESSAGE_LENGTH)
                        .build()
                        .start();
                endpoint.awaitTermination();
            } catch (IOException e) {
                messageHandler.error(e, "Error launching communication server");
            }catch (InterruptedException e){
                messageHandler.message("Client communication service stopped");
            }finally {
                if (endpoint!=null) endpoint.shutdownNow();
            }
        });
        communication_server.start();
        serviceThreads.add(communication_server);
    }


    /**<pre>
     * This method will perform a update request near the manager in the hope of receiving an update on the trust relationships created by the manager and that the current nodes must obey
     * </pre>
     */
    static void periodicUpdater(Client client) {

        //Acquire the semaphore to prevent a new update request to be issued while the previous update was not yet concluded
        try{
            client.updateSemaphore.acquire();
        } catch (InterruptedException e) {
            client.messageHandler.error(e,"Error acquiring semaphore ");
            return;
        }

        try {

            client.messageHandler.message("Performing chain update");

            /*
                Open a communication channel directed to the pool manager and sending the request through it
             */
            byte[] communicateResult = client.communicate("manager." + client.pool,Configuration.MANAGER_PORT, UpdateProcessorPTASC.processorName, UpdateProcessorPTASC.input(client));

            /*
                Process the response by repopulating the fields and file contents with the newly received information
             */
            if (communicateResult != null) {


                JSONObject jsonObject = new JSONObject(new String(communicateResult));

                X509Certificate mCert = null;
                try {
                    mCert = (X509Certificate) CertificateFactory.getInstance("X509").generateCertificate(new ByteArrayInputStream(java.util.Base64.getDecoder().decode(jsonObject.getString("cert"))));
                } catch (CertificateException e) {
                    client.messageHandler.error(e,"Error getting certificate from the update response");
                }


                JSONArray certs = jsonObject.getJSONArray("certsChain");
                List<X509Certificate> updatesToTheCertificateChain = new LinkedList<>();

                List<String> certStringList = new ArrayList<>();
                for(int i=0; i < certs.length(); i++) {
                    certStringList.add(certs.getString(i));
                }

                for (String certB64 : certStringList) {
                    try {
                        updatesToTheCertificateChain.add((X509Certificate) CertificateFactory.getInstance("X509").generateCertificate(new ByteArrayInputStream(java.util.Base64.getDecoder().decode(certB64))));
                    } catch (CertificateException e) {
                        client.messageHandler.error(e,"Error getting certsChain certificates from the update response");
                    }
                }

                X509Certificate myCert = (mCert == null) ? (client.myCertificate[0]) : (mCert);
                client.myCertificate = new X509Certificate[1 + updatesToTheCertificateChain.size()];
                client.myCertificate[0] = myCert;
                for (int i = 0; i < updatesToTheCertificateChain.size(); i++) {
                    client.myCertificate[i + 1] = updatesToTheCertificateChain.get(i);
                }


                JSONArray certs2 = jsonObject.getJSONArray("trust");

                List<String> cert2StringList = new ArrayList<>();
                for(int i=0; i < certs2.length(); i++) {
                    cert2StringList.add(certs2.getString(i));
                }

                client.trustAnchors = new LinkedList<>();
                for (String certB64 : cert2StringList) {
                    try {
                        client.trustAnchors.add((X509Certificate) CertificateFactory.getInstance("X509").generateCertificate(new ByteArrayInputStream(java.util.Base64.getDecoder().decode(certB64))));
                    } catch (CertificateException e) {
                        client.messageHandler.error(e,"Error getting trust certificates from the update response");
                    }
                }


                /*
                    Update the stored certificate information, by converting the received certificate chain to PEM
                 */
                StringBuilder toWrite = new StringBuilder();

                for(X509Certificate crt : client.myCertificate){
                    toWrite.append("\n").append(Utils.convertX509CertificateToPEM(client, crt));
                }

                FileWriter f = new FileWriter(client.certificateFile);
                f.write(toWrite.toString());
                f.flush();
                f.close();

                client.messageHandler.message("Update finished with success");

            } else {
                client.messageHandler.error(null,"Update problem");
            }
        }catch (Exception e){
            client.messageHandler.error(e,"Update problem");
        }finally {
            client.updateSemaphore.release();
        }

    }


    /**Allows/Blocks the execution of update requests
     * @param allowUpdates The value to be updated to the allowUpdates flag
     */
    public void setAllowUpdates(boolean allowUpdates) {
        this.allowUpdates = allowUpdates;
    }
}
