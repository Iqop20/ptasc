package inconnu.ptasc.communication.processors;

import inconnu.ptasc.PTascNode;
import inconnu.ptasc.communication.processors.utils.OrionForwardingHandler;
import org.json.JSONObject;
import java.io.IOException;
import java.security.cert.X509Certificate;

/**
 * This in the main processor of this implementation being responsible to communicate with Fiware Orion with authentication and access control measures deployed
 */
public class OrionProcessorPTASC extends PTASCCommunicationProcessor {
    String orionURL;
    String authzforceURL;

    public static String processorName = "orion";

    /** Initializes the processor and sets the FIWARE Orion URL to be used
     * @param orionURL The FIWARE Orion URL to be used
     */
    public OrionProcessorPTASC(String orionURL) {
        super(processorName);
        this.orionURL = orionURL;
    }

    /** This method encodes the several pieces of the usual Orion HTTP request into o byte array to be sent via the gRPC communication channel
     * @param body The JSON request body
     * @param method The HTTP method of the request
     * @param url The Orion URL to be requested
     * @return A byte[] containing the request encoded in a way that the process() method can decode
     */
    public static byte[] input(JSONObject body, String method, String url){
        JSONObject output = new JSONObject();
        output.put("url",url);
        output.put("method",method);
        output.put("body",body.toString());

        return output.toString().getBytes();
    }


    /** The method does the authorization verification on AuthzForce and request forwarding to Orion, returning the response received from Orion
     * @param input The bytes received as input
     * @param senderCertificate The certificate of the sender
     * @param current The current device information
     * @return byte[] containing the response to be sent back to the sender
     * @throws IOException If some error rises
     */
    @Override
    public byte[] process(byte[] input, X509Certificate senderCertificate, PTascNode current) throws IOException {
        /*
           Authorize the request near authzforce
           Generate an Orion request, send it and catch the response
        */
        OrionForwardingHandler orionForwardingHandler = new OrionForwardingHandler(current, input, senderCertificate, orionURL, (current.authzURL==null)?(this.authzforceURL):(current.authzURL));

        if (orionForwardingHandler.authorizeRequest()) {
            try {
                OrionForwardingHandler.OrionRequestResult orionRequestResult = orionForwardingHandler.doRequest();
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("code",orionRequestResult.code);
                jsonObject.put("data",orionRequestResult.data);

                return jsonObject.toString().getBytes();
            } catch (IOException e) {
                current.messageHandler.error(e, "Error forwarding message to ORION");
                throw e;
            }
        }
        return null;
    }
}
