package inconnu.ptasc.communication.processors;

import inconnu.ptasc.PTascNode;
import inconnu.ptasc.Utils;
import inconnu.ptasc.client.Client;
import org.apache.commons.lang3.tuple.Pair;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Objects;

/**
 * This processor is responsible for processing update requests
 */
public class UpdateProcessorPTASC extends PTASCCommunicationProcessor {

    public static String processorName = "update";

    public UpdateProcessorPTASC() {
        super(processorName);
    }

    /** Builds the update input encoded to be processed by the process() method
     * @param client The current device
     * @return A byte[] containing the input ready to be placed on the communication channel
     * @throws IOException If some error on the generation of the CSR an exception is thrown
     */
    public static byte[] input(Client client) throws IOException {
        JSONObject request = new JSONObject();
        request.put("command", "update");
        request.put("csr", java.util.Base64.getEncoder().encodeToString(Objects.requireNonNull(Utils.generateCSR(client, client.ECDSApubKey, client.ECDSAprivKey, client.name + "." + client.pool)).getEncoded()));

        return request.toString().getBytes();
    }

    /** Processes the input and performs the update procedure, returning the resulting certificate chain encoded in a byte[]
     * @param input The bytes received as input
     * @param senderCertificate The certificate of the sender
     * @param current The current device information
     * @return byte[] containing the response to be sent back to the sender
     * @throws IOException If some error rises
     */
    @Override
    public byte[] process(byte[] input, X509Certificate senderCertificate, PTascNode current) throws IOException {

        JSONObject request = new JSONObject(new String(input));
        String senderUSSN = senderCertificate.getSubjectX500Principal().getName().split("CN=")[1];
        String senderName = senderUSSN.split("\\.")[0];
        String senderPool = senderUSSN.split(senderName + "\\.")[1];

        JSONObject response = new JSONObject();

        /*
            Only answer if the current device is a manager device that manages the pool where the sender belongs
         */
        if (current.name.equals("manager") && senderPool.equals(current.pool)) {

            /*
                Signing a new csr coming from the same public key enables
            */
            try {
                PKCS10CertificationRequest certificationRequest = new PKCS10CertificationRequest(java.util.Base64.getDecoder().decode(request.getString("csr")));


                if (certificationRequest.getSubject().toString().split("CN=")[1].equals(senderUSSN)) {

                    X509Certificate certificate = Utils.signCSR(current, certificationRequest, current.ECDSAprivKey, new BigInteger(32, new SecureRandom()), current.name + "." + current.pool, current.myCertificate[0], 365 * 24 * 60 * 60, null);
                    if (certificate != null) {
                        if (Arrays.equals(certificate.getPublicKey().getEncoded(), senderCertificate.getPublicKey().getEncoded())) {
                            response.put("cert", java.util.Base64.getEncoder().encodeToString(certificate.getEncoded()));
                        } else {
                            current.messageHandler.error(null, "CSR public key does not match the senders public key");
                            throw new IOException("CSR public key does not match the senders public key");
                        }
                    }
                } else {
                    current.messageHandler.error(null, "CSR does not match the sender USSN");
                    throw new IOException("CSR does not match the sender USSN");
                }
            } catch (IOException | CertificateEncodingException e) {
                current.messageHandler.error(e, "Error reading CSR and processing it on update request");
            }


            /*
                Build a JSON response containing an array with PEM encoded certificate to update the node with a new certification chain
                Throw error and exit if some problem occurs
            */
            try {
                JSONArray jsonArray = new JSONArray();
                if (current.myCertificate != null) {
                    for (X509Certificate crt : current.myCertificate) {
                        jsonArray.put(java.util.Base64.getEncoder().encodeToString(crt.getEncoded()));
                    }
                }
                response.put("certsChain", jsonArray);
            } catch (CertificateEncodingException e) {
                current.messageHandler.error(e, "Error encoding certificates from myCertificate chain");
                throw new IOException("Error encoding certificates from myCertificate chain"+'\n'+e.getMessage());

            }

            /*
               Also send the trusted chain to update the node of the new trust relationships
            */
            try {
                JSONArray jsonArray = new JSONArray();
                if (current.trustAnchors != null) {
                    for (X509Certificate crt : current.trustAnchors) {
                        jsonArray.put(java.util.Base64.getEncoder().encodeToString(crt.getEncoded()));
                    }
                }
                response.put("trust", jsonArray);
            } catch (CertificateEncodingException e) {
                current.messageHandler.error(e, "Error encoding certificates from trust chain");
                throw new IOException("Error encoding certificates from trust chain"+'\n'+e.getMessage());

            }
        }
        return response.toString().getBytes();
    }
}
