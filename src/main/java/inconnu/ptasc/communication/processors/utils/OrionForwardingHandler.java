package inconnu.ptasc.communication.processors.utils;

import com.squareup.okhttp.*;
import inconnu.ptasc.PTascNode;
import inconnu.ptasc.Utils;
import org.json.JSONObject;
import java.io.IOException;
import java.security.cert.X509Certificate;

/**
 * This class contains functions for authorization verification near the Authzforce instance and forwarding of authorized requests to the Orion instance
 * This is an element of the OrionProcessorPTASC processor
 */
public class OrionForwardingHandler {

    //Request body
    String url;
    String method;
    String body;

    //Redirection information
    String orionURL;
    String authzURL;



    //Current device and sender information
    PTascNode current;
    X509Certificate senderCertificate;

    boolean authorized;

    /** Initializes an instance of this handler, granting access to the authorizeRequest() and doRequest() methods
     * @param current The current device object
     * @param message The message to be forwarded
     * @param senderCertificate The X509Certificate received from the sender and authenticated by the communication protocol
     * @param orionUrl The URL on which FIWARE Orion is listening into
     * @param authzURL The URL on which the FIWARE Authzforce is listening into
     */
    public OrionForwardingHandler(PTascNode current,byte[] message, X509Certificate senderCertificate, String orionUrl, String authzURL) {
        this.current=current;
        JSONObject requestJSON = new JSONObject(new String(message));
        url = requestJSON.getString("url");
        method = requestJSON.getString("method");
        body = requestJSON.getString("body");
        this.orionURL = orionUrl;
        this.authorized = false;
        this.senderCertificate = senderCertificate;
        this.authzURL = authzURL;
    }


    /** This method fetches information from the message to be forwarded to build a PDP request forwarded to Authzforce
     * @return The result from Authzforce, true if granted and false otherwise
     */
    public boolean authorizeRequest(){

        String USSN = senderCertificate.getSubjectX500Principal().getName().split("CN=")[1];
        String authzDomainId=Utils.getAuthzDomainIDFromIssuerAlternativeName(current,senderCertificate);
        String entityID=null;

        if (url.startsWith("/v2/entities")) {
        /*
            Getting the resource id from the url
            If the url does not have id information, use the request body
         */
            String[] components = url.split("/v2/entities");
            if (url.startsWith("/v2/entities") && url.length() > "/v2/entities".length()) {
                String[] subcomponents = components[1].split("/");
                if (subcomponents.length >= 2) {
                    entityID = subcomponents[1];
                }
            }

            if (entityID == null) {
                try {
                    JSONObject jsonObject = new JSONObject(body);
                    entityID = jsonObject.getString("id");
                } catch (Exception e) {
                    current.messageHandler.error(e,"Error getting entityID from /v2/entities body");
                    entityID = "none";
                }
            }
        }else if (url.startsWith("/v2/subscriptions")){
            /*
                Getting the information from the POST body of the subscription
                Deletion is currently forbidden
             */
            if (method.equals("POST")){
                JSONObject bodyJson = new JSONObject(body);
                JSONObject entities = bodyJson.getJSONObject("subject").getJSONArray("entities").getJSONObject(0);
                if (entities.has("id")){
                    entityID = entities.getString("id");
                }else if (entities.has("idPattern")){
                    entityID = entities.getString("idPattern");
                }else entityID="none";
            }else entityID="none";
        }else entityID="none";

        return (authorized = Utils.requestPermission(current,authzDomainId,authzURL, USSN, url, entityID,method));
    }

    /**This method performs a request to Orion if it was previously authorized
     * The method builds an HTTP request using the url on the message and placing as HTTP body the received body
     * @return The request result, including the HTTP code and the response body
     * @throws IOException If some error on the request forwarding is issued
     */
    public OrionRequestResult doRequest() throws IOException {
        if (authorized) {
            Request.Builder url = new Request.Builder()
                    .url(orionURL + this.url);
            if (method.equals("DELETE")){
                url.delete();
            }else{
                url.method(method, (body == null || body.equals("")) ? (null) : (RequestBody.create(MediaType.parse("application/json"), body)));
            }

            Request orionRequest = url.build();

            OkHttpClient client = new OkHttpClient();
            Response response = client.newCall(orionRequest).execute();

            String data;
            if (this.url.startsWith("/v2/subscriptions")){
                data = response.header("Location");
            }else data = response.body().string();

            response.body().close();
            return new OrionRequestResult(data,response.code());
        }else throw new IOException("Authorize First");
    }

    /**
     * Data structure to hold the request result
     */
    public static  class OrionRequestResult{
        public String data;
        public  int code;

        /**
         * @param data The response data
         * @param code The response HTTP status code
         */
        public OrionRequestResult(String data, int code) {
            this.data = data;
            this.code = code;
        }
    }

}
