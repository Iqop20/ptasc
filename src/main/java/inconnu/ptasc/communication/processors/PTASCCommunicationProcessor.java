package inconnu.ptasc.communication.processors;

import inconnu.ptasc.PTascNode;

import java.io.IOException;
import java.security.cert.X509Certificate;

/**
 * This is the basic communication processor structure, the process method is called by the communication protocol if the processorName on the request matches the processor name on the processor
 */
public abstract class PTASCCommunicationProcessor {
    public String processorName;
    public PTASCCommunicationProcessor(String processorName) {
        super();
        this.processorName = processorName;
    }

    /**
     * @param input The bytes received as input
     * @param senderCertificate The certificate of the sender
     * @param current The current device information
     * @return byte[] containing the response to be sent back to the sender
     * @throws IOException If some error rises
     */
    public abstract byte[] process(byte[] input, X509Certificate senderCertificate, PTascNode current) throws IOException;
}
