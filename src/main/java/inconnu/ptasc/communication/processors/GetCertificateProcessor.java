package inconnu.ptasc.communication.processors;

import inconnu.ptasc.PTascNode;
import java.io.IOException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;

/**
 * This processor returns the device certificate to the sender
 */
public class GetCertificateProcessor extends PTASCCommunicationProcessor{

    public static final String processorName="get-cert";

    public GetCertificateProcessor() {
        super(processorName);
    }


    /**Produces the input to be used by the process() method
     * @return An empty byte array, since this processor does not need any input
     */
    public static byte[] input(){
        return "".getBytes();
    }


    /** Returns the device certificate
     * @param input The bytes received as input
     * @param senderCertificate The certificate of the sender
     * @param current The current device information
     * @return byte[] containing the response to be sent back to the sender
     * @throws IOException If some error rises
     */
    @Override
    public byte[] process(byte[] input, X509Certificate senderCertificate, PTascNode current) throws IOException {

        try {
            return current.myCertificate[0].getEncoded();
        } catch (CertificateEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }
}
