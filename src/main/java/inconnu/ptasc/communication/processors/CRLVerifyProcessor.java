package inconnu.ptasc.communication.processors;

import inconnu.crl.Client;
import inconnu.crl.Configuration;
import inconnu.ptasc.PTascNode;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;

/**This processor is responsible to forward CRL verification requests from device on the city-internet <br>
 *This processor is already in place on the endpoint device
 */
public class CRLVerifyProcessor extends PTASCCommunicationProcessor{


    public static String processorName = "crl-verify";
    public CRLVerifyProcessor() {
        super(processorName);
    }

    /** This method produces the input that is interpreted by the process() method of this processor
     * @param certificateToVerify The X509Certificate to be verified against the CRL serverdatabase
     * @return A byte[] to be used on the communication channel as input
     * @throws Exception
     */
    public static byte[] input(X509Certificate certificateToVerify) throws Exception {
        return Base64.getEncoder().encode(certificateToVerify.getEncoded());
    }

    /** This method uses the input received and uses the CRL client to perform a CRL verification request on the CRL server, returning the result
     * @param input The bytes received as input
     * @param senderCertificate The certificate of the sender
     * @param current The current device information
     * @return byte[] containing the response to be sent back to the sender
     * @throws IOException If some error rises
     */
    @Override
    public byte[] process(byte[] input, X509Certificate senderCertificate, PTascNode current) throws IOException {
        try {
            X509Certificate certToVerify = (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(Base64.getDecoder().decode(input)));
            return Client.verify(certToVerify, current.crlIp, Configuration.SERVER_PORT).getBytes();
        } catch (CertificateException e) {
            throw new IOException(e);
        }
    }
}
