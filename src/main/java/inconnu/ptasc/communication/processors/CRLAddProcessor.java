package inconnu.ptasc.communication.processors;

import inconnu.crl.Configuration;
import inconnu.ptasc.PTascNode;
import org.json.JSONObject;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;

/** This processor is responsible to forward CRL addition requests from the sub-managers<br>
 * This processor is already in place on the endpoint device
 */
public class CRLAddProcessor extends PTASCCommunicationProcessor{

    public static String processorName = "crl-add";

    public CRLAddProcessor() {
        super(processorName);
    }

    /** Preparing the input to be sent on the request, this input is tailored to the process method of this processor as the process method will run on the endpoint device
     * @param managerCertificate The certificate of the manager that is doing the revoke request
     * @param managerPrivateKey The private key of the manager associated with the managerCertificate
     * @param certificateToRevoke The client certificate to revoke
     * @return a byte[] containing the input to be sent
     * @throws Exception If the signature of the request fails or the device is not manager an exception is thrown
     */
    public static byte[] input(X509Certificate managerCertificate, PrivateKey managerPrivateKey, X509Certificate certificateToRevoke) throws Exception {

        if (managerCertificate.getSubjectX500Principal().getName().startsWith("manager.")) {

            Signature signature = Signature.getInstance("SHA256withECDSA");
            signature.initSign(managerPrivateKey);
            signature.update(MessageDigest.getInstance("SHA256").digest(certificateToRevoke.getEncoded()));
            byte[] signRes = signature.sign();

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("mc", Base64.getEncoder().encodeToString(managerCertificate.getEncoded()));
            jsonObject.put("mh", Base64.getEncoder().encodeToString(signRes));
            jsonObject.put("cr", Base64.getEncoder().encodeToString(certificateToRevoke.getEncoded()));

            return jsonObject.toString().getBytes();
        }else throw new Exception("Not a manager");
    }

    /**This method reads the input and invokes the CRL client to perform the request near the CRL server
     * @param inputBytes The bytes received as input
     * @param senderCertificate The certificate of the sender
     * @param current The current device information
     * @return byte[] containing the response to be sent back to the sender
     * @throws IOException If some error rises
     */
    @Override
    public byte[] process(byte[] inputBytes, X509Certificate senderCertificate, PTascNode current) throws IOException {
        JSONObject input = new JSONObject(inputBytes);

        try {
            X509Certificate managerCert = (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(Base64.getDecoder().decode(input.getString("mc"))));
            byte[] hash = Base64.getDecoder().decode(input.getString("mh"));
            X509Certificate certificateToRevoke = (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(Base64.getDecoder().decode(input.getString("cr"))));
            if (managerCert.getSubjectX500Principal().getName().startsWith("manager.")) {
                return inconnu.crl.Client.add(managerCert, hash, certificateToRevoke, current.crlIp, Configuration.SERVER_PORT).getBytes();
            } else throw new IOException("Not manager");
        }catch (Exception e) {
            throw new IOException(e);
        }
    }
}
