package inconnu.ptasc.communication;

import com.google.protobuf.ByteString;
import inconnu.crl.Client;
import inconnu.ptasc.Configuration;
import inconnu.ptasc.PTascNode;
import inconnu.ptasc.Utils;
import inconnu.ptasc.communication.processors.CRLVerifyProcessor;
import inconnu.ptasc.communication.processors.PTASCCommunicationProcessor;
import inconnu.ptasc.grpc.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import org.apache.commons.lang3.tuple.Pair;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import org.bouncycastle.crypto.util.SubjectPublicKeyInfoFactory;
import org.bouncycastle.math.ec.ECPoint;
import java.io.*;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Objects;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


/**
 * This class contains methods and classes that can be used as communication client (communicate() method) and communication server (CommunicationService class) allowing communication between devices on the architecture
 */
public class Communication {


    /**This method is used to initialize the communication with some device that has its communication server open and ready to accept requests using the selected processor
     * @param destinationUSSN The destination of the communication DNS name
     * @param destinationPORT The destination port
     * @param processorName The processor name selected
     * @param message The message produced by the processor input() method
     * @param current The current device information
     * @return A byte[] containing the communication output
     * @throws IOException If some issue on the communication arises an exception is thrown
     */
    public static byte[] communicate(String destinationUSSN, int destinationPORT, String processorName, byte[] message, PTascNode current) throws IOException {
        /*
            Generating the ECIES key pair containing the public and private key
            Encoding and signing ECIES pub Key, the signature will be made with the ECDSA priv Key that is associated with the public key signed on myCertificate
            The signed result will be sent to destinationUSSN and will be used to set the shared symmetricKey that will be used to communicate
        */
        byte[] ECIESpubKeySignedWithECDSA;
        AsymmetricCipherKeyPair ECIESKeyPair = Utils.generateECIESKeyPair();
        byte[] ECIESpubKeyEncoded;
        try {
            ECIESpubKeyEncoded = SubjectPublicKeyInfoFactory.createSubjectPublicKeyInfo(ECIESKeyPair.getPublic()).getEncoded();
            ECIESpubKeySignedWithECDSA = Utils.signWithECDSASHA256(current,ECIESpubKeyEncoded, current.ECDSAprivKey);
            assert ECIESpubKeySignedWithECDSA!=null;
        } catch (IOException e) {
            current.messageHandler.error(e,"Failed to encode and sign ECIESpubKey");
            return null;
        }

        /*
            Create a PEM encoded certificate chain
            That chain will be used by the receiver node to verify if the current node is trusted
        */
        StringBuilder s=new StringBuilder();
        for(X509Certificate crt: current.myCertificate){
            s.append(Utils.convertX509CertificateToPEM(current,crt));
        }
        String certBundle = s.toString();


        //Open a GRPC channel to the communication service of the destinationUSSN
        ManagedChannel channel = ManagedChannelBuilder.forTarget(destinationUSSN+":"+destinationPORT)
                .maxInboundMessageSize(Configuration.MAX_MESSAGE_LENGTH)
                .usePlaintext()
                .build();
        CommunicationGrpc.CommunicationStub communicationStub = CommunicationGrpc.newStub(channel).withDeadlineAfter(10,TimeUnit.MINUTES);



        /*
            These latches are used to signal the end of authentication and end of communication respectively
         */
        CountDownLatch waitForServerCountdownLatch = new CountDownLatch(1);
        CountDownLatch authenticationCountdownLatch = new CountDownLatch(1);

        /*
            Communication steps information placeholders that make information accessible to the stream observer and to the current method
         */
        final AuthenticationResultInformationBundle authResultInfo = new AuthenticationResultInformationBundle();
        final CommunicationResultInformationBundle communicationResultInformationBundle = new CommunicationResultInformationBundle();

        Vector<byte[]> input = new Vector<>();
        /*
            Send the requests and process the responses
         */
        StreamObserver<CommunicationRequest> communicate = communicationStub.communicate(new StreamObserver<CommunicationResponse>() {
            @Override
            public void onNext(CommunicationResponse communicationResponse) {
                switch (communicationResponse.getResponseCase().getNumber()) {
                    case 1: //Authentication Response
                        /*
                            Processes Authentication response
                         */
                        AuthenticationResponse authResp = communicationResponse.getAuthResp();

                        //Get receiver certificate bundle encoded into PEM format
                        byte[] receiverCertificateBundle = authResp.getReceiverSignedCertificate().toByteArray();

                        try {
                            //Decode the certificate bundle and store it into an array of certficates
                            X509Certificate[] receiverBundle = CertificateFactory.getInstance("X.509").generateCertificates(new ByteArrayInputStream(receiverCertificateBundle)).toArray(new X509Certificate[0]);

                            //Check the received certificate CN, if the CN differs from destinationUSSN the communication must be aborted (possible MiTM attack)
                            if (receiverBundle[0].getSubjectX500Principal().getName().equals("CN=" + destinationUSSN)) {

                                //Verify if the received certificate bundle is trusted by checking the matches with the current node trust anchors
                                if (Utils.verifyCertificateInTrustChain(current,current.trustAnchors, receiverBundle)) {


                                    /*
                                        Get the R sent on the response and check if the signature matches the received value
                                        The signature is checked using the receiver certificate public key
                                        If the signature matches, then the information is stored on a AuthenticationResultInformationBundle object, the information stored will be used on the encrypted communication step
                                    */
                                    byte[] RencodedBytes = authResp.getR().toByteArray();
                                    if (Utils.verifyECDSASHA256Signature(current, authResp.getSignedR().toByteArray(), RencodedBytes, receiverBundle[0].getPublicKey())) {
                                        authResultInfo.R = ((ECPublicKeyParameters) ECIESKeyPair.getPublic()).getParameters().getCurve().decodePoint(RencodedBytes);
                                        authResultInfo.receiverCertificate = receiverBundle[0];
                                        authResultInfo.salt = authResp.getSalt().toByteArray();
                                        //Generate the aesKey and store the result
                                        authResultInfo.aesKey = Utils.receiverGenerateECIESSymmetricKey(current, authResultInfo.R, ((ECPrivateKeyParameters)ECIESKeyPair.getPrivate()).getD(), authResultInfo.salt);
                                    } else {
                                        current.messageHandler.error(null, "R signature failed to be verified, authentication failed");
                                    }

                                } else {
                                    current.messageHandler.error(null,"Certificate verification failed, quiting authentication");
                                }
                            }else{
                                current.messageHandler.error(null,"Received certificate chain does not match the destination USSN");
                            }
                        } catch (CertificateException e) {
                            current.messageHandler.error(e,"Received certificate chain can not be decoded");
                        }
                        //Mark authentication as complete
                        authenticationCountdownLatch.countDown();
                        break;
                    case 2: //Encrypted Communication Response
                        /*
                            Process encrypted communication responses
                         */
                        CommResponse commResp = communicationResponse.getCommResp();

                        /*
                            Get the encrypted message information from the response
                         */
                        EncryptedMessage encryptedResponse = commResp.getMessage();
                        byte[] iv = encryptedResponse.getIv().toByteArray();
                        byte[] encryptedMessage = encryptedResponse.getEncryptedMessage().toByteArray();
                        byte[] signatute = commResp.getSignature().toByteArray();

                        //Verify if the encrypted message received matches its signature, the signature is verified using the certificate of the responder
                        if (Utils.verifyECDSASHA256Signature(current,signatute,encryptedMessage,authResultInfo.receiverCertificate.getPublicKey())){
                            //Decipher the message contents and store them on a CommunicationResultInformationBundle accessible outside the stream observer
                            byte[] decipheredMessageWithAES256 = Utils.decipherMessageWithAES256(current,authResultInfo.aesKey, iv, encryptedMessage);
                            if (decipheredMessageWithAES256==null){
                                current.messageHandler.error(null,"Communication answer decryption returned a null value");
                            }else {
                                input.add(decipheredMessageWithAES256);
                            }
                        }else{
                            current.messageHandler.error(null,"Failed to verify signature on the received encrypted message");
                        }
                        break;
                }
            }

            @Override
            public void onError(Throwable throwable) {
                //When an error is received mark all transmissions as complete and print an error
                waitForServerCountdownLatch.countDown();
                authenticationCountdownLatch.countDown();
                current.messageHandler.error(new Exception(throwable),"Communication error");
            }

            @Override
            public void onCompleted() {
                //When the server marks the transmission as complete, also mark the communication as complete
                waitForServerCountdownLatch.countDown();
                authenticationCountdownLatch.countDown();

                int length =0;
                for (byte[] v : input){
                    length+=v.length;
                }


                ByteArrayOutputStream bos = new ByteArrayOutputStream(length);

                for(byte[] v : input){
                    try {
                        bos.write(v);
                        bos.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                communicationResultInformationBundle.communicationResult =bos.toByteArray();
            }
        });

        /*
            Perform an authentication request and wait for response
         */
        AuthenticationRequest authenticationRequest = AuthenticationRequest.newBuilder()
                .setSignedECIESPubKey(ByteString.copyFrom(ECIESpubKeySignedWithECDSA))
                .setSenderSignedCertificate(ByteString.copyFrom(certBundle.getBytes()))
                .setECIESPubKey(ByteString.copyFrom(ECIESpubKeyEncoded))
                .build();
        communicate.onNext(CommunicationRequest.newBuilder().setAuthReq(authenticationRequest).build());
        try {
            authenticationCountdownLatch.await();
        } catch (InterruptedException e) {
            current.messageHandler.error(e,"Authentication latch problem");
            return null;
        }

        /*
            If the aesKey is null the authentication was not successful, therefore communication has failed
         */
        if (authResultInfo.aesKey!=null) {


            //Cipher the message
            Pair<byte[], byte[]> messageEncryptionResult = Utils.cipherMessageWithAES256(current, authResultInfo.aesKey, message);

            /*
                If the encryption fails a message cannot be sent and communication fails
             */
            if (messageEncryptionResult != null) {

                /*
                    Get the message encryption information
                    Signe the encrypted message with the private key associated with the current node certificate
                 */
                byte[] iv = messageEncryptionResult.getLeft();
                byte[] encryptionResult = messageEncryptionResult.getRight();
                byte[] signatureResult = Utils.signWithECDSASHA256(current, encryptionResult, current.ECDSAprivKey);

                /*
                    If signature fails the message cannot be sent and the communication fails
                 */
                if (signatureResult != null) {

                    /*
                        Encapsulate the encrypted message into a GRPC communication request message and send it
                     */
                    EncryptedMessage encryptedMessage = EncryptedMessage.newBuilder()
                            .setIv(ByteString.copyFrom(iv))
                            .setEncryptedMessage(ByteString.copyFrom(encryptionResult))
                            .build();

                    CommRequest commRequest = CommRequest.newBuilder()
                            .setProcessorName(processorName)
                            .setMessage(encryptedMessage)
                            .setSignature(ByteString.copyFrom(signatureResult))
                            .build();

                    communicate.onNext(CommunicationRequest.newBuilder().setCommReq(commRequest).build());

                } else current.messageHandler.error(null, "Failed to sign the encrypted message");
            } else current.messageHandler.error(null, "Failed to cipher message");

            communicate.onCompleted();


            //Wait for response
            try {
                waitForServerCountdownLatch.await();
            } catch (InterruptedException e) {
                current.messageHandler.error(e,"Server wait latch problem");
                return null;
            }

            //Return the communication result, this value can be null if some error occurs on the response processing
            channel.shutdown();
            while(!channel.isShutdown()) {
                try {
                    channel.awaitTermination(5, TimeUnit.SECONDS);
                } catch (InterruptedException e) {
                    current.messageHandler.error(e,"Error terminating communication channel");
                }
            }
            return communicationResultInformationBundle.communicationResult;
        }else {
            current.messageHandler.error(null,"Symmetric Key could not be established");
        }
        channel.shutdown();
        while(!channel.isShutdown()) {
            try {
                channel.awaitTermination(5, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                current.messageHandler.error(e,"Error terminating communication channel");
            }
        }
        return null;
    }


    /**
     * Data structure to capture the authentication information
     */
    static class AuthenticationResultInformationBundle{
        public X509Certificate receiverCertificate;
        public ECPoint R;
        public byte[] salt;
        public  byte[] aesKey;

        public AuthenticationResultInformationBundle() {
            super();
        }
    }

    /**
     * Data structure to capture the communication result
     */
    public static class CommunicationResultInformationBundle{
        public byte[] communicationResult;
        public CommunicationResultInformationBundle() {
            super();
        }
    }


    /**<pre>
     * Class that enables to answer communication requests
     * </pre>
     */
    public static class CommunicationService extends CommunicationGrpc.CommunicationImplBase{

        PTascNode current;
        String processorName;


        /** Initializes the communication service making the device ready to answer communication requests
         * @param current The current node
         */
        public CommunicationService(PTascNode current) {
            this.current = current;
        }

        /**<pre>
         * This method will allow to answer communication requests, forwarding them to Fiware Orion if requested
         * Both request and responses are encrypted and signed
         * </pre>
         * @param responseObserver A response observer that allows to respond to the request
         * @return A stream observer that allows to process the incoming request
         */
        @Override
        public StreamObserver<CommunicationRequest> communicate(StreamObserver<CommunicationResponse> responseObserver) {
            return new CommunicationProcessor(responseObserver);

        }


        class CommunicationProcessor implements StreamObserver<CommunicationRequest>{
            StreamObserver<CommunicationResponse> responseObserver;
            byte[] aesKey;
            X509Certificate senderCertificate;
            Vector<byte[]> input;
            public CommunicationProcessor(StreamObserver<CommunicationResponse> responseObserver) {
                super();
                input = new Vector<>();
                this.responseObserver= responseObserver;
            }

            @Override
            public void onNext(CommunicationRequest communicationRequest) {
                switch (communicationRequest.getRequestCase().getNumber()) {
                    case 1: //Authentication request
                        /*
                            Process authentication request
                         */
                        AuthenticationRequest authReq = communicationRequest.getAuthReq();
                        //Get the sender certificate bundle encoded in PEM
                        byte[] senderCertificateBundleBytes = authReq.getSenderSignedCertificate().toByteArray();

                        /*
                            Decode the PEM certificate bundle and store it as a X509Certificate array
                            Throw error and exit if some error occurs
                         */
                        X509Certificate[] senderCertBundle;
                        try {
                            senderCertBundle = CertificateFactory.getInstance("X.509").generateCertificates(new ByteArrayInputStream(senderCertificateBundleBytes)).toArray(new X509Certificate[0]);
                            senderCertificate = senderCertBundle[0];
                        } catch (CertificateException e) {
                            current.messageHandler.error(e, "Error getting certificate");
                            responseObserver.onError(Status.ABORTED.withDescription(e.getMessage()).asRuntimeException());
                            return;
                        }

                        /*
                            Verify the certificate bundle to check if the certificate is trusted using the current node trust-anchors
                         */
                        if (!Utils.verifyCertificateInTrustChain(current, current.trustAnchors, senderCertBundle)) {
                            current.messageHandler.error(null, "Certificate chain validation failed");
                            responseObserver.onError(Status.ABORTED.withDescription("Certificate chain validation failed").asRuntimeException());
                            return;
                        }

                        if (current.needsToContactExternalCRL){
                            current.messageHandler.message("Using remote");
                            try {
                                byte[] communicate = Communication.communicate(current.crlIp, Configuration.DEVICE_COMMUNICATION_PORT, CRLVerifyProcessor.processorName, CRLVerifyProcessor.input(senderCertificate), current);
                                if (communicate!=null) {
                                    String v = new String(communicate);
                                    if (!v.equals("not found")) {
                                        current.messageHandler.error(null, "CRL verification returned " + v);
                                        responseObserver.onError(Status.ABORTED.withDescription("CRL verification returned " + v).asRuntimeException());
                                        return;
                                    }
                                }else throw  new Exception("Null return");
                            } catch (Exception e) {
                                current.messageHandler.error(null, "CRL verification returned " + e.getMessage());
                                responseObserver.onError(Status.ABORTED.withDescription("CRL verification returned " + e.getMessage()).asRuntimeException());
                                return;
                            }
                        }else {
                            current.messageHandler.message("Using local");
                            String verify;
                            try {
                                verify = Client.verify(senderCertificate, current.crlIp, inconnu.crl.Configuration.SERVER_PORT);
                            } catch (CertificateEncodingException e) {
                                current.messageHandler.error(null, "CRL verification returned " + e.getMessage());
                                responseObserver.onError(Status.ABORTED.withDescription("CRL verification returned " + e.getMessage()).asRuntimeException());
                                return;
                            }

                            if (!verify.equals("not found")) {
                                current.messageHandler.error(null, "CRL verification returned " + verify);
                                responseObserver.onError(Status.ABORTED.withDescription("CRL verification returned " + verify).asRuntimeException());
                                return;
                            }
                        }

                        /*
                            Get the sender ECIES public key and verify its signature using the received certificate public key
                            Throw error and exit if some error occurs
                         */
                        PublicKey senderPublicKey = senderCertBundle[0].getPublicKey();
                        byte[] ECIESpubKeyBytes = authReq.getECIESPubKey().toByteArray();
                        if (!Utils.verifyECDSASHA256Signature(current, authReq.getSignedECIESPubKey().toByteArray(), ECIESpubKeyBytes, senderPublicKey)) {
                            current.messageHandler.error(null, "ECIES public key signature verification failed");
                            responseObserver.onError(Status.ABORTED.withDescription("ECIES public key signature verification failed").asRuntimeException());
                            return;
                        }

                        /*
                            Decode the ECIES public key
                            Throw error and exit if some error occurs
                         */
                        ECPublicKeyParameters senderECIESPubKey;
                        try {
                            senderECIESPubKey = (ECPublicKeyParameters) PublicKeyFactory.createKey(ECIESpubKeyBytes);
                        } catch (IOException e) {
                            current.messageHandler.error(e, "Error decoding sender ECIES public key");
                            responseObserver.onError(Status.ABORTED.withDescription(e.getMessage()).asRuntimeException());
                            return;
                        }

                        /*
                            Generate a random salt and generate ECIES key agreement R parameter
                            Throw error and exit if the parameter fails to be generated
                         */
                        byte[] salt = new byte[32];
                        (new SecureRandom()).nextBytes(salt);
                        Pair<ECPoint, byte[]> symmetricKeyGenerationResult = Utils.senderGenerateECIESSymmetricKey(current, senderECIESPubKey.getQ(), senderECIESPubKey.getParameters().getG(), salt);

                        if (symmetricKeyGenerationResult != null) {

                            ECPoint R = symmetricKeyGenerationResult.getLeft();
                            aesKey = symmetricKeyGenerationResult.getRight();

                            /*
                                Create a PEM encoded certificate chain
                                That chain will be used by the receiver node to verify if the current node is trusted
                            */
                            StringBuilder s = new StringBuilder();
                            for (X509Certificate crt : current.myCertificate) {
                                s.append(Utils.convertX509CertificateToPEM(current, crt));
                            }
                            String certBundle = s.toString();


                            /*
                                Send the authentication response containing the R parameter that will be used by the sender to build a symmetric key that is equal to aesKey
                             */
                            AuthenticationResponse authenticationResponse = AuthenticationResponse.newBuilder()
                                    .setSalt(ByteString.copyFrom(salt))
                                    .setR(ByteString.copyFrom(R.getEncoded(false)))
                                    .setReceiverSignedCertificate(ByteString.copyFrom(certBundle.getBytes()))
                                    .setSignedR(ByteString.copyFrom(Objects.requireNonNull(Utils.signWithECDSASHA256(current, R.getEncoded(false), current.ECDSAprivKey))))
                                    .build();

                            responseObserver.onNext(CommunicationResponse.newBuilder().setAuthResp(authenticationResponse).build());
                        } else {
                            current.messageHandler.error(null, "Problem on Symmetric Key Generation");
                            responseObserver.onError(Status.ABORTED.withDescription("Problem on Symmetric Key Generation").asRuntimeException());

                        }


                        break;
                    case 2: //Encrypted Communication request
                        /*
                            Process encrypted communication request
                         */
                        CommRequest commReq = communicationRequest.getCommReq();
                        /*
                            Get the encrypted message information
                         */
                        byte[] iv = commReq.getMessage().getIv().toByteArray();
                        byte[] encryptedMessage = commReq.getMessage().getEncryptedMessage().toByteArray();
                        byte[] encryptedMessageSignature = commReq.getSignature().toByteArray();
                        processorName =commReq.getProcessorName();

                        /*
                            Verify the encrypted message signature to attest if the origin of the message is indeed the sender and not a MiTM attacker
                            If the signature fails throw error and exit
                         */
                        if (!Utils.verifyECDSASHA256Signature(current, encryptedMessageSignature, encryptedMessage, senderCertificate.getPublicKey())) {
                            current.messageHandler.error(null, "Encrypted message signature verification failed");
                            responseObserver.onError(Status.ABORTED.withDescription("Encrypted message signature verification failed").asRuntimeException());
                            return;
                        }


                        /*
                            Decipher the encrypted message to process it
                            If the decipher process fails throw error and exit
                         */
                        byte[] decipheredMessage = Utils.decipherMessageWithAES256(current, aesKey, iv, encryptedMessage);
                        if (decipheredMessage == null) {
                            current.messageHandler.error(null, "Message deciphering error");
                            responseObserver.onError(Status.ABORTED.withDescription("Message deciphering error").asRuntimeException());
                            return;
                        }

                        input.add(decipheredMessage);

                }
            }

            @Override
            public void onError(Throwable throwable) {

            }

            @Override
            public void onCompleted() {
                boolean processed = false;

                for (PTASCCommunicationProcessor processor : current.getProcessors()) {
                    if (processor.processorName.equals(processorName)) {
                        processed = true;
                        byte[] result = null;
                        try {
                            int length = 0;
                            for(byte[] v : input){
                                length+=v.length;
                            }

                            ByteArrayOutputStream bos = new ByteArrayOutputStream(length);

                            for(byte[] v : input){
                                bos.write(v);
                                bos.flush();
                            }
                            bos.close();
                            result = processor.process(bos.toByteArray(), senderCertificate, current);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        if (result != null) {


                            /*
                                Encrypt the orion response
                                Throw error and exit if some problem occurs
                            */
                            Pair<byte[], byte[]> orionResponseEncryptionResult = Utils.cipherMessageWithAES256(current, aesKey, result);
                            if (orionResponseEncryptionResult == null) {
                                current.messageHandler.error(null, "Unable to cipher the ORION response");
                                responseObserver.onError(Status.ABORTED.withDescription("Unable to cipher the ORION response").asRuntimeException());
                                return;
                            }

                            /*
                                Get encryption information and sign the encrypted message using the private key associated with the current node certificate
                                Throw an error and exit if some error occurs on the signature
                             */
                            byte[] orionIV = orionResponseEncryptionResult.getLeft();
                            byte[] orionResponseCiphered = orionResponseEncryptionResult.getRight();
                            byte[] orionResponseCipheredSignature = Utils.signWithECDSASHA256(current, orionResponseCiphered, current.ECDSAprivKey);
                            if (orionResponseCipheredSignature == null) {
                                current.messageHandler.error(null, "Unable to sign the encrypted ORION response");
                                responseObserver.onError(Status.ABORTED.withDescription("Unable to sign the encrypted ORION response").asRuntimeException());
                                return;
                            }


                            /*
                                Send the communication response to the requester node
                             */
                            CommResponse commResponse = CommResponse.newBuilder()
                                    .setMessage(EncryptedMessage.newBuilder()
                                            .setIv(ByteString.copyFrom(orionIV))
                                            .setEncryptedMessage(ByteString.copyFrom(orionResponseCiphered))
                                            .build())
                                    .setSignature(ByteString.copyFrom(orionResponseCipheredSignature))
                                    .build();
                            responseObserver.onNext(CommunicationResponse.newBuilder().setCommResp(commResponse).build());


                            //Mark transmission as complete
                            responseObserver.onCompleted();
                            break;
                        }else {
                            current.messageHandler.error(null, "Processor " + processor + " failed to provide output");
                            responseObserver.onError(Status.ABORTED.withDescription("Processor " + processor + " failed to provide output").asRuntimeException());
                        }
                        break;
                    }
                }

                if (!processed){
                    current.messageHandler.error(null, "No processor was called");
                    responseObserver.onError(Status.ABORTED.withDescription("No processor was called").asRuntimeException());
                }

            }

        }
    }


}
