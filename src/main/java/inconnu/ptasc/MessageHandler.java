package inconnu.ptasc;

/**
 * Basic structure for the message handler
 * The message handler allows for custom behaviour on issuing errors and messages
 */
public abstract class MessageHandler {
    public abstract void error(Exception e,String message);
    public abstract void message(String message);
}
