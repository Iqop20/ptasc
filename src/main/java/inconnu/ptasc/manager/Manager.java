package inconnu.ptasc.manager;


import com.google.common.io.ByteStreams;
import inconnu.ptasc.*;
import inconnu.ptasc.communication.Communication;
import inconnu.ptasc.communication.processors.GetCertificateProcessor;
import inconnu.ptasc.communication.processors.UpdateProcessorPTASC;
import io.grpc.Server;
import io.grpc.netty.shaded.io.grpc.netty.NettyServerBuilder;
import org.apache.commons.lang3.tuple.Pair;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import org.bouncycastle.crypto.util.SubjectPublicKeyInfoFactory;
import org.json.JSONObject;
import java.io.*;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.concurrent.Semaphore;

/**
 * This class initializes the manager devices<br>
 * When a Manager is initialized is it ready to accept provisioning requests and updates
 */
public class Manager extends PTascNode {

    String managedPoolName;
    public File otpFile;
    public Semaphore otpSemaphore = new Semaphore(1);


    /** This method instantiates a manager device that can be a sub-manager or city manager
     * @param managedPoolName The name of the pool to manage or the name of the city domain in the case of city-manager
     * @param ManagerECDSApubKeyFile The file that will store the manager ECDSA public key
     * @param ManagerECDSAprivKeyFile The file that will store the manager ECDSA private key
     * @param ManagerCertificateFile The file that will store the manager certificate
     * @param ECIESpubKeyFile The file that will store the ECIES public key that is used for the provisioning process
     * @param ECIESprivKeyFile The file that will store the ECIES private key that is used for the provisioning process
     * @param QRfile The file that will store the QR code image containing the ECDSA and ECIES public keys encoded on it
     * @param handler The message and error output handler, allowing to customize the way how that information is shown to the administrator
     * @param ECDSAb64 The file that will store the ECDSA public key encoded in base64
     * @param ECIESb64 The file that will store the ECIES public key encoded in base64
     * @param crlIp The address of the CRL server, if the manager is a sub-manager it usually connects to the CRL server via the endpoint device
     * @param needsToContactExternalCRL On the deployment the CRL server is in the Endpoint device localhost, but is external for any other client. This flag sets the need to contact the external CRL via the endpoint device - endpointUSSN -.
     * @param otpFile The file where the otp public id and secret keys of all the yubikeys supported are added
     * @throws IOException If there is an error during the instantiation an error is thrown
     */
    public Manager(String managedPoolName,File ManagerECDSApubKeyFile,File ManagerECDSAprivKeyFile,File ManagerCertificateFile,File ECIESpubKeyFile,File ECIESprivKeyFile,File QRfile,MessageHandler handler,File ECDSAb64,File ECIESb64,String crlIp,boolean needsToContactExternalCRL,File otpFile) throws IOException {
        super(handler);
        authzURL=null;

        this.name = "manager";
        this.managedPoolName = managedPoolName;
        this.pool = managedPoolName;
        this.crlIp = crlIp;
        this.needsToContactExternalCRL = needsToContactExternalCRL;
        this.otpFile = otpFile;


        /*
            If all files are available it means that the manager was already instantiated before
            Otherwise if some file is not available restart all the instantiation process
         */
        if (!ManagerECDSAprivKeyFile.exists() || !ManagerECDSApubKeyFile.exists() || !ECIESprivKeyFile.exists() || !ECIESpubKeyFile.exists() ||!ManagerCertificateFile.exists()) {

            ManagerECDSApubKeyFile.delete();
            ManagerECDSAprivKeyFile.delete();
            ECIESprivKeyFile.delete();
            ECIESpubKeyFile.delete();
            ManagerCertificateFile.delete();

            /*
                Generate public and private key pair (Manager keys)
                Signing
                EC 384
             */
            KeyPair keyPair = Utils.generateECDSAKeyPair(this,384);
            if (keyPair != null) {
                ECDSApubKey = keyPair.getPublic();
                ECDSAprivKey = keyPair.getPrivate();

                ManagerECDSApubKeyFile.createNewFile();
                ManagerECDSAprivKeyFile.createNewFile();

                FileOutputStream fisPub = new FileOutputStream(ManagerECDSApubKeyFile);
                FileOutputStream fisPriv = new FileOutputStream(ManagerECDSAprivKeyFile);
                fisPriv.write(ECDSAprivKey.getEncoded());
                fisPub.write(ECDSApubKey.getEncoded());
                fisPriv.close();
                fisPub.close();

            } else return;

            //Manager Self Signed Certificate
            myCertificate = new X509Certificate[1];
            myCertificate[0] = Utils.signCSR(this,Objects.requireNonNull(Utils.generateCSR(this,ECDSApubKey,ECDSAprivKey, "manager." + managedPoolName)),ECDSAprivKey,new BigInteger("1"),"manager."+managedPoolName,null,365*24*60*60,null);
            //Manager trusts its own certificate
            trustAnchors = new LinkedList<>();
            trustAnchors.add(myCertificate[0]);
            ManagerCertificateFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(ManagerCertificateFile);
            fos.write(Utils.convertX509CertificateToPEM(this,myCertificate[0]).getBytes());
            fos.flush();
            fos.close();


            /*
                Generate public and private key pair (ECIES)
                Encryption
                EC 384
             */
            AsymmetricCipherKeyPair keys = Utils.generateECIESKeyPair();
            this.ECIESprivKey = (ECPrivateKeyParameters) keys.getPrivate();
            this.ECIESpubKey = (ECPublicKeyParameters) keys.getPublic();
            this.G = this.ECIESpubKey.getParameters().getG();

            ECIESprivKeyFile.createNewFile();
            ECIESpubKeyFile.createNewFile();
            FileOutputStream fisPub = new FileOutputStream(ECIESpubKeyFile);
            FileOutputStream fisPriv = new FileOutputStream(ECIESprivKeyFile);

            fisPub.write(SubjectPublicKeyInfoFactory.createSubjectPublicKeyInfo(ECIESpubKey).getEncoded());
            fisPriv.write(ECIESprivKey.getD().toByteArray());

            fisPub.close();
            fisPriv.close();

            messageHandler.message("Key generation successful");
        }else{

            try {
                FileInputStream fis = new FileInputStream(ManagerECDSApubKeyFile);
                byte[] ManagerpubKeyBytes = ByteStreams.toByteArray(fis);
                fis.close();
                ECDSApubKey = KeyFactory.getInstance("EC").generatePublic(new X509EncodedKeySpec(ManagerpubKeyBytes));

                fis = new FileInputStream(ManagerECDSAprivKeyFile);
                byte[] ManagerprivKeyBytes = ByteStreams.toByteArray(fis);
                fis.close();
                ECDSAprivKey = KeyFactory.getInstance("EC").generatePrivate(new PKCS8EncodedKeySpec(ManagerprivKeyBytes));

                fis=new FileInputStream(ManagerCertificateFile);

                CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
                X509Certificate[] certificates = certFactory.generateCertificates(fis).toArray(new X509Certificate[0]);

                trustAnchors = new LinkedList<>();
                X509Certificate mycert=null;
                for(X509Certificate certificate : certificates){
                    String subjectName = certificate.getSubjectX500Principal().getName();
                    if (subjectName.equals("CN=manager." + managedPoolName)){
                        mycert = certificate;
                    }else trustAnchors.add(certificate);
                }
                myCertificate = new X509Certificate[trustAnchors.size()+1];
                myCertificate[0] = mycert;
                for(int i =0;i< trustAnchors.size();i++){
                    myCertificate[i+1] = trustAnchors.get(i);
                }
                trustAnchors.add(mycert);

                fis.close();

                fis = new FileInputStream(ECIESpubKeyFile);
                byte[] ECIESpubKeyBytes =ByteStreams.toByteArray(fis);
                fis.close();
                this.ECIESpubKey = (ECPublicKeyParameters) PublicKeyFactory.createKey(ECIESpubKeyBytes);

                fis = new FileInputStream(ECIESprivKeyFile);
                byte[] ECIESprivKeyBytes = ByteStreams.toByteArray(fis);
                fis.close();
                BigInteger D = new BigInteger(ECIESprivKeyBytes);
                this.ECIESprivKey = new ECPrivateKeyParameters(D,this.ECIESpubKey.getParameters());


                messageHandler.message("Successfully read keys from files");

            } catch (NoSuchAlgorithmException | InvalidKeySpecException | CertificateException e) {
                messageHandler.error(e,"Error reading keys from files");
                return;
            }
        }


        JSONObject jsonObject = new JSONObject();
        jsonObject.put("CAECDSA",java.util.Base64.getEncoder().encode(ECDSApubKey.getEncoded()));
        jsonObject.put("CAECIES",java.util.Base64.getEncoder().encode(SubjectPublicKeyInfoFactory.createSubjectPublicKeyInfo(ECIESpubKey).getEncoded()));

        Utils.writeQrToFile(this,jsonObject.toString(),QRfile);

        handler.message(Configuration.PTASC_LOG+"ECDSA public key: "+java.util.Base64.getEncoder().encodeToString(ECDSApubKey.getEncoded()));
        handler.message(Configuration.PTASC_LOG+"ECIES public key: "+java.util.Base64.getEncoder().encodeToString(SubjectPublicKeyInfoFactory.createSubjectPublicKeyInfo(ECIESpubKey).getEncoded()));

        if (ECDSAb64!=null) new FileOutputStream(ECDSAb64).write(java.util.Base64.getEncoder().encode(ECDSApubKey.getEncoded()));
        if (ECIESb64!=null) new FileOutputStream(ECIESb64).write(java.util.Base64.getEncoder().encode(SubjectPublicKeyInfoFactory.createSubjectPublicKeyInfo(ECIESpubKey).getEncoded()));



        /*
            Initialize the manager services
         */

        Thread provisioningThread = new Thread(() -> {
            Server provisioning=null;
            try {
                //Initialize GRPC server
                this.addCommunicationProcessor(new UpdateProcessorPTASC());
                this.addCommunicationProcessor(new GetCertificateProcessor());
                provisioning = NettyServerBuilder.forPort(Configuration.MANAGER_PORT)
                        .addService(new Provisioning.ManagerProvisioningService(this, managedPoolName))
                        .addService(new Communication.CommunicationService(this))
                        .intercept(new ExceptionHandler())
                        .maxInboundMessageSize(Configuration.MAX_MESSAGE_LENGTH)
                        .build()
                        .start();

                this.messageHandler.message("Manager now listening for requests");
                provisioning.awaitTermination();
            } catch (IOException e) {
                this.messageHandler.error(e, "Error starting provisioning service");
            }catch (InterruptedException e){
                this.messageHandler.message("Provisioning service stopped");
            }finally {
                if (provisioning!=null) provisioning.shutdownNow();

            }
        });
        provisioningThread.start();
        serviceThreads.add(provisioningThread);
    }


    /**
     *  Attempts to make an appending request near a manager, the result is that this manager will become a client to the upper manager.<br>
     *  All the nodes managed by this node will be able to communicate with all the nodes that trust the upper manager
     *
     * @param poolToJoin The name of the pool to join (i.e. city)
     * @param otherManagerECIESpubKey The ECIES public key of the poolToJoin manager
     * @param otherManagerECDSApubKey The ECDSA public key of the poolToJoin manager
     * @param otp The otp generated by the poolToJoin manager yubikey
     */
    public boolean appendToOtherManagerPool(String poolToJoin, ECPublicKeyParameters otherManagerECIESpubKey, PublicKey otherManagerECDSApubKey, String otp,File managerCertificate){
        Pair<X509Certificate, List<X509Certificate>> manager = provision(otherManagerECIESpubKey,otherManagerECDSApubKey, "manager."+managedPoolName, poolToJoin, otp,managerCertificate);
        if (manager!=null) {
            trustAnchors = manager.getRight();
            myCertificate = new X509Certificate[trustAnchors.size() + 1];
            myCertificate[0] = manager.getLeft();
            for (int i = 0; i < trustAnchors.size(); i++) {
                myCertificate[i + 1] = trustAnchors.get(i);
            }
            trustAnchors.add(myCertificate[0]);
            return true;
        }else {
            messageHandler.error(null,"Error on appending");
            return false;
        }
    }


    /**This method adds a yubikey token to the trusted tokens enabling provisioning when using that yubikey
     * @param publicId The public YubicoOTP id
     * @param aesSecret The YubicoOTP aes secret associated with the public id
     * @throws IOException If there is an error placing the YubicoOTP information throw an exception
     */
    public void addToken(String publicId,String aesSecret) throws IOException {
        try {
            otpSemaphore.acquire();
            Scanner scanner = new Scanner(otpFile);
            boolean found=false;
            while(scanner.hasNext() && !found){
                found = scanner.nextLine().startsWith(publicId);
            }
            if (!found) {
                FileWriter fileWriter = new FileWriter(otpFile, true);
                fileWriter.write(publicId + ":" + aesSecret+":-1:-1");
                fileWriter.close();
            }
            otpSemaphore.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
