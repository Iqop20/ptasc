package inconnu.ptasc;

/**
 * This MessageHandler issues errors and messages using System.out.println() method printing errors and messages on the terminal
 */
public class SystemOutPrintlnMessageHandler extends MessageHandler{
    @Override
    public void error(Exception e, String message) {
        if (e!=null){
            System.out.println(Configuration.PTASC_LOG+"ERROR "+message+"due to "+e.getMessage());
        }else System.out.println(Configuration.PTASC_LOG+"ERROR "+message);
    }
    @Override
    public void message(String message) {
        System.out.println(Configuration.PTASC_LOG+"Message: "+message);
    }
}
