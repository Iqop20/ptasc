package inconnu.ptasc;

import com.google.common.io.ByteStreams;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.squareup.okhttp.*;
import inconnu.ptasc.manager.Manager;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.tuple.Pair;
import org.bouncycastle.asn1.sec.SECNamedCurves;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.generators.ECKeyPairGenerator;
import org.bouncycastle.crypto.generators.PKCS5S2ParametersGenerator;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECKeyGenerationParameters;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.util.PrivateKeyFactory;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.DefaultDigestAlgorithmIdentifierFinder;
import org.bouncycastle.operator.DefaultSignatureAlgorithmIdentifierFinder;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.bc.BcECContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.x500.X500Principal;
import java.io.*;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.*;
import java.util.*;


/**
 * A set of methods to perform a range of operations
 */
public class Utils {

    /**<pre>
     * This method reads a specific amount of bytes from a input stream
     * If the specified amount is not available the method returns what is capable to read before reaching the end of the input stream
     * </pre>
     * @param inputStream The input stream to read from
     * @param amount The amount to read
     * @return A byte array containing the number of bytes read from the input stream, the length is always <= amount
     * @throws IOException If there is some problem reading from input stream
     */
    public static byte[] readBytesFromInputStream(InputStream inputStream, int amount) throws IOException {
        byte[] content = new byte[amount];
        int bytesRead;
        int alreadyRead = 0;
        while (amount != alreadyRead && (bytesRead = inputStream.read(content, alreadyRead, amount - alreadyRead)) != -1) {
            alreadyRead += bytesRead;
        }

        return Arrays.copyOfRange(content,0,alreadyRead);
    }



    /** This method signs the contents of a message using SHA256withECDSA
     * @param current The current device
     * @param message The message to sign
     * @param privateKey The ECDSA private key that will do the signature
     * @return The signed content in a byte[] or null if an exception occurs
     */
    public static byte[] signWithECDSASHA256(PTascNode current,byte[] message, PrivateKey privateKey){

        try {
            Signature signature = Signature.getInstance("SHA256withECDSA");
            signature.initSign(privateKey);
            signature.update(message);
            return signature.sign();
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
            current.messageHandler.error(e,"Error signing with SHA256");
        }

        return null;
    }

    /** This method verifies a signature to check if the message contents were altered in any way
     * @param current The current device
     * @param signedMessage The message signature to be verified
     * @param message The message that was signed
     * @param publicKey The public key associated with the private key that signed the message
     * @return A boolean stating if the signature matches the message contents or not
     */
    public static boolean verifyECDSASHA256Signature(PTascNode current,byte[] signedMessage,byte[] message,PublicKey publicKey){
        try {
            Signature signature = Signature.getInstance("SHA256withECDSA");
            signature.initVerify(publicKey);
            signature.update(message);
            return signature.verify(signedMessage);
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
            current.messageHandler.error(e,"Error verifying signature with SHA256");
        }

        return false;
    }


    /** This method enables the sender - the device that starts the communication - to generate a symmetric AES key given ECIES key pair parameters
     * The returned information is the elliptic curve point R and the AES key in bytes
     * @param current The current device
     * @param Q The ECIES public key of the receiver
     * @param G The generator point of the receiver elliptic curve
     * @param salt A random byte[] that will be used as salt on the AES key generation
     * @return A pair containing the ECIES protocol R and the AES key in bytes, or null if an error occurs (more information: https://asecuritysite.com/encryption/ecc3)
     */
    public static Pair<ECPoint, byte[]> senderGenerateECIESSymmetricKey(PTascNode current,ECPoint Q,ECPoint G,byte[] salt){
        BigInteger r = new BigInteger(384,new SecureRandom());

        ECPoint R = G.multiply(r).normalize();
        ECPoint S = Q.multiply(r).normalize();

        byte[] aesKey = generateAESKeyFromECPoint(current,S,salt);

        if (aesKey!=null)
            return Pair.of(R,aesKey);
        else return null;
    }

    /** This method enables a receiver - the device that receives the communication - to generate the same symmetric AES key as the sender without sending the key through the communication channel
     * @param current The current device
     * @param R The sender R
     * @param d The current node ECIES private key
     * @param salt A byte[] with the same salt as the sender
     * @return An byte[] containing the AES key, or a null in case of an error
     */
    public static byte[] receiverGenerateECIESSymmetricKey(PTascNode current,ECPoint R,BigInteger d,byte[] salt){
        ECPoint S = R.multiply(d).normalize();
        return generateAESKeyFromECPoint(current,S,salt);
    }

    /** This method extracts information of an elliptic curve point and uses PBKDF2 algorithm to produce an AES 256-bit symmetric key
     * @param current The current device
     * @param S The EC point resulted from the ECIES protocol
     * @param salt A random byte[] that both sender and receiver have knowledge of
     * @return A byte[] containing the AES key or a null in case of an error
     */
    public static byte[] generateAESKeyFromECPoint(PTascNode current,ECPoint S,byte[] salt){
        //Get S encoded and compressed
        byte[] encoded = S.getEncoded(true);

        try {

            PKCS5S2ParametersGenerator gen = new PKCS5S2ParametersGenerator(new SHA256Digest());
            gen.init(java.util.Base64.getEncoder().encode(encoded), salt, 1000);
            return ((KeyParameter) gen.generateDerivedParameters(256)).getKey();

        } catch (Exception e) {
            current.messageHandler.error(e,"Error generating AES key from EC point");
            return null;
        }
    }

    /** This method uses an AES key, generates a random IV and ciphers message returning the ciphered result
     * @param current The current device
     * @param key The AES key to be used
     * @param message The message to be ciphered
     * @return A pair containing the IV generated and the ciphered message
     */
    public static Pair<byte[],byte[]> cipherMessageWithAES256(PTascNode current,byte[] key, byte[] message){
        try {

            //Generate CBC iv
            int ivSize = 16;
            byte[] iv = new byte[ivSize];
            SecureRandom random = new SecureRandom();
            random.nextBytes(iv);
            IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);


            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

            //Convert key to AES format
            SecretKeySpec aes = new SecretKeySpec(key, "AES");

            cipher.init(Cipher.ENCRYPT_MODE,aes,ivParameterSpec);

            return Pair.of(iv,cipher.doFinal(message));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            current.messageHandler.error(e,"Error ciphering message with AES256");
        }

        return null;
    }

    /** This method deciphers and message using an AES key and an IV returning the deciphered result
     * @param current The current device
     * @param key The AES key to be used
     * @param iv The IV to be used
     * @param ciphertext The ciphered message
     * @return A byte[] containing the deciphered contents or null in case of error
     */
    public static byte[] decipherMessageWithAES256(PTascNode current,byte[] key,byte[] iv, byte[] ciphertext){
        try {

            //Convert IV to a parameter spec
            IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);


            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

            //Convert key to AES format
            SecretKeySpec aes = new SecretKeySpec(key, "AES");

            cipher.init(Cipher.DECRYPT_MODE,aes,ivParameterSpec);

            return cipher.doFinal(ciphertext);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            current.messageHandler.error(e,"Error deciphering message with AES256");
        }

        return null;
    }

    /** This method generates an ECDSA key with key size passed as argument
     * @param current The current device
     * @param size The ECDSA key size
     * @return The generated ECDSA key pair
     */
    public static KeyPair generateECDSAKeyPair(PTascNode current,int size){

        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("EC");
            keyPairGenerator.initialize(size, new SecureRandom());
            return keyPairGenerator.generateKeyPair();

        } catch (NoSuchAlgorithmException e) {
            current.messageHandler.error(e,"Error generating ECDSA key pair");
        }
        return null;
    }

    /** Generates an ECIES key pair
     * @return The ECIES key pair
     */
    public static AsymmetricCipherKeyPair generateECIESKeyPair(){
        X9ECParameters ecp = SECNamedCurves.getByName("secp384r1");
        ECDomainParameters domainParams = new ECDomainParameters(ecp.getCurve(), ecp.getG(), ecp.getN(), ecp.getH(), ecp.getSeed());

        ECKeyGenerationParameters keyGenParams = new ECKeyGenerationParameters(domainParams, new SecureRandom());

        ECKeyPairGenerator generator = new ECKeyPairGenerator();

        generator.init(keyGenParams);
        return generator.generateKeyPair();
    }

    /** This method generates a CSR that is then signed by a manager with the signCSR method
     * @param current  The current device
     * @param publicKey The ECDSA public key to be part of the certificate
     * @param privateKey The ECDSA private key associated to the public key, it is used to sign the CSR
     * @param CN The current device common name, that will be its USSN
     * @return The CSR generated or null is case of an error
     */
    public static PKCS10CertificationRequest generateCSR(PTascNode current,PublicKey publicKey, PrivateKey privateKey, String CN) {
        try {
            PKCS10CertificationRequestBuilder p10Builder = new JcaPKCS10CertificationRequestBuilder(
                    new X500Principal("CN=" + CN), publicKey);
            JcaContentSignerBuilder csBuilder = new JcaContentSignerBuilder("SHA256withECDSA");
            ContentSigner signer = csBuilder.build(privateKey);
            return p10Builder.build(signer);
        } catch (OperatorCreationException e) {
            current.messageHandler.error(e,"Error generating CSR");
        }

        return null;
    }

    /** This method extracts information from the CSR and builds a signed certificate, enabling the sender device to be provisioned in the process
     * @param current The current device
     * @param certificationRequest The CSR received
     * @param CAprivKey The current device ECDSA private key
     * @param serialNumber The serial number to be used on the certificate
     * @param signerCN The common name of the current device
     * @param signerCert The certificate of the current device
     * @param maxValiditySeconds The max validity of the certificate
     * @param authzDomainId The domain ID of the pool where the sender device will be part of, usually is the same ID as the manager. This information is embedded on the certificate
     * @return The signed certificate or null in case of an error
     */
    public static X509Certificate signCSR(PTascNode current,PKCS10CertificationRequest certificationRequest,PrivateKey CAprivKey,BigInteger serialNumber,String signerCN,X509Certificate signerCert,long maxValiditySeconds,String authzDomainId){
        try {
            X509v3CertificateBuilder certificateBuilder = new X509v3CertificateBuilder(
                    new X500Name("CN="+signerCN),
                    serialNumber, //serial number
                    new Date(System.currentTimeMillis()), //validity starts at
                    new Date(System.currentTimeMillis() + maxValiditySeconds*1000), //validity ends at
                    certificationRequest.getSubject(),
                    certificationRequest.getSubjectPublicKeyInfo()
            );

            if (signerCert!=null) {
                certificateBuilder.addExtension(Extension.authorityKeyIdentifier,false,
                        new AuthorityKeyIdentifier(
                                signerCert.getPublicKey().getEncoded(),
                                new GeneralNames(new GeneralName(new X500Name(signerCert.getSubjectX500Principal().getName()))),
                                signerCert.getSerialNumber()));
                if (authzDomainId!=null){
                    certificateBuilder.addExtension(Extension.issuerAlternativeName,false,new GeneralNames(new GeneralName(GeneralName.rfc822Name,authzDomainId)));
                }else{
                    authzDomainId = Utils.getAuthzDomainIDFromIssuerAlternativeName(current,signerCert);
                    if (authzDomainId!=null){
                        certificateBuilder.addExtension(Extension.issuerAlternativeName,false,new GeneralNames(new GeneralName(GeneralName.rfc822Name,authzDomainId)));
                    }
                }
            }

            certificateBuilder.addExtension(Extension.basicConstraints,true,new BasicConstraints(certificationRequest.getSubject().toString().contains("manager")));


            AlgorithmIdentifier sigAlgId = new DefaultSignatureAlgorithmIdentifierFinder().find("SHA256withECDSA");
            AlgorithmIdentifier digAlgId = new DefaultDigestAlgorithmIdentifierFinder().find(sigAlgId);

            AsymmetricKeyParameter privKey = PrivateKeyFactory.createKey(CAprivKey.getEncoded());

            ContentSigner signer = new BcECContentSignerBuilder(sigAlgId, digAlgId).build(privKey);


            X509CertificateHolder certificateHolder = certificateBuilder.build(signer);
            org.bouncycastle.asn1.x509.Certificate eeX509CertificateStructure = certificateHolder.toASN1Structure();

            CertificateFactory cf = CertificateFactory.getInstance("X.509");

            return (X509Certificate) cf.generateCertificate(new ByteArrayInputStream(eeX509CertificateStructure.getEncoded()));
        } catch (IOException | CertificateException | OperatorCreationException e) {
            current.messageHandler.error(e,"Error signing CSR");
        }

        return null;
    }

    /** This method converts the certificate into a human-readable PEM format
     * @param current The current device
     * @param x509Certificate The certificate to encode into PEM
     * @return The PEM encoded result or null in case of error
     */
    public static String convertX509CertificateToPEM(PTascNode current,X509Certificate x509Certificate){
        try {
            StringWriter r = new StringWriter();
            JcaPEMWriter writer = new JcaPEMWriter(r);
            writer.writeObject(x509Certificate);
            writer.flush();
            writer.close();
            return r.toString();
        }catch (Exception e){
            current.messageHandler.error(e,"Error converting X509 certificate to PEM encoding");
        }
        return null;
    }

    /** This method, used by managers, enables the verification of the validity of the OTP token following the YubicoOTP protocol, described on the dissertation
     * @param manager The manager device
     * @param otp The received OTP
     * @return An boolean stating if the OTP is valid or not
     */
    public static boolean isOTPValid(Manager manager, String otp){

        String pubid = otp.substring(0, 12);
        String encryptedHexString = otp.substring(12);

        try {
            manager.otpSemaphore.acquire();
        } catch (InterruptedException e) {
            manager.messageHandler.error(e,"OTP validation failed, semaphore problem ");
            return false;
        }
        //Open otp file
        Scanner scanner;
        try {
            scanner = new Scanner(manager.otpFile);
        } catch (FileNotFoundException e) {
            manager.messageHandler.error(e,"OTP validation failed, unable to open file");
            return false;
        }

        manager.otpSemaphore.release();

        //Get aes key
        String aesKey = null;
        int prevUsageCounter=-1;
        int prevSessionCounter=-1;
        while (scanner.hasNext() && aesKey == null) {
            String s = scanner.nextLine();
            if (s.startsWith(pubid)) {
                aesKey = s.split(":")[1];
                prevUsageCounter = Integer.parseInt(s.split(":")[2]);
                prevSessionCounter = Integer.parseInt(s.split(":")[3]);
            }
        }
        scanner.close();

        if (aesKey == null) {
            manager.messageHandler.error(new Exception("aesKey is null"),"OTP validation failed");
            return false;
        }

        byte[] encryptedBytes;
        try {
            encryptedBytes = modHexDecode(encryptedHexString);
        } catch (Exception e) {
            manager.messageHandler.error(e,"OTP validation failed, unable to modHexDecode");
            return false;
        }

        byte[] decipheredOtp;
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
            cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(Hex.decodeHex(aesKey), "AES"));
            decipheredOtp = cipher.doFinal(encryptedBytes);
        } catch (NoSuchPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException | BadPaddingException | InvalidKeyException | DecoderException e) {
            manager.messageHandler.error(e,"OTP validation failed, problem deciphering encrypted otp");
            return false;
        }

        if (decipheredOtp == null) {
            manager.messageHandler.error(new Exception("Decryption result is null"),"OTP validation failed");
            return false;
        }


        if (Utils.crc16(decipheredOtp)!=0xf0b8){
            manager.messageHandler.error(new Exception("CRC validation failed, result not equal to 0xF0B8"),"OTP validation failed");
            return false;
        }

        int usageCounter = new BigInteger(Arrays.copyOfRange(decipheredOtp, 6, 8)).intValue();
        int sessionCounter = new BigInteger(Arrays.copyOfRange(decipheredOtp,11,12)).intValue();

        if (usageCounter>prevUsageCounter || (usageCounter==prevUsageCounter && sessionCounter>prevSessionCounter)){

            try {
                manager.otpSemaphore.acquire();
                scanner = new Scanner(manager.otpFile);
                StringBuilder newUse=new StringBuilder();
                while (scanner.hasNext()){
                    String s = scanner.nextLine();
                    if (s.startsWith(pubid)){
                        newUse.append(pubid).append(":").append(aesKey).append(":").append(usageCounter).append(":").append(sessionCounter).append('\n');
                    }else newUse.append(s);
                }
                scanner.close();
                FileWriter fileWriter = new FileWriter(manager.otpFile);
                fileWriter.write(newUse.toString());
                fileWriter.flush();
                fileWriter.close();

                manager.otpSemaphore.release();
                return true;

            } catch (IOException e) {
                manager.messageHandler.error(e,"OTP validation failed, unable to write updated counters");
                manager.otpSemaphore.release();
                return false;
            } catch (InterruptedException e) {
                manager.messageHandler.error(e,"OTP validation failed, unable to acquire semaphore to update counters");
                return false;
            }

        }



        return false;

    }

    /** This method is used to generate an CRC16 checksum that is required by YubicOTP protocol to check for OTP corruption
     * @param toCheck The byte[] contents to check
     * @return The 16-bit CRC16 checksum
     */
    public static int crc16(byte[] toCheck){
        int crc = 0xffff;

        for (byte b : toCheck){
            crc ^= b & 0xFF;
            for (int i=0;i<8;i++){
                short j = (short) (crc & 1);
                crc >>= 1;
                if (j>0){
                    crc ^= 0x8408;
                }
            }
        }
        return crc;
    }


    /** This method decodes the OTP using the modHEX algorithm (https://developers.yubico.com/OTP/Modhex_Converter.html)
     * @param encoded The encoded OTP
     * @return The decoded data
     * @throws Exception An exception is thrown if the algorithm fails
     */
    public static byte[] modHexDecode(String encoded) throws Exception {
        String modhexBase = "cbdefghijklnrtuv";
        String modHex = encoded.replaceAll("[^cbdefghijklnrtuv]","c");

        if(modHex.length() % 2 !=0){
            throw new Exception("ModHex has no even length");
        }

        StringBuilder hexOut = new StringBuilder();

        for(int i=0;i<modHex.length();i++){
            hexOut.append(Integer.toHexString(modhexBase.indexOf(modHex.charAt(i))));
        }


        return Hex.decodeHex(hexOut.toString());

    }


    /** This method checks the certification chain of the received certificate and attempts to match one of the certificates on the chain with the trust anchors in place for the current device
     * @param current  The current device
     * @param trustChain The current device trust anchors
     * @param certBundle The received certificate
     * @return A boolean stating if the certificate is trusted or not
     */
    public static boolean verifyCertificateInTrustChain(PTascNode current,List<X509Certificate> trustChain,X509Certificate[] certBundle){
        if (trustChain!=null){
            try {
                CertPathValidator cpv = CertPathValidator.getInstance("PKIX");
                Set<TrustAnchor> trustAnchors = new HashSet<>();
                for (X509Certificate trustedCert : trustChain) {
                    trustAnchors.add(new TrustAnchor(trustedCert,null));
                }
                PKIXParameters params = new PKIXParameters(trustAnchors);
                params.setRevocationEnabled(false);

                CertPath certPath = CertificateFactory.getInstance("X.509").generateCertPath(Arrays.asList(certBundle));

                cpv.validate(certPath,params);
                return true;
            } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException | CertificateException | CertPathValidatorException e) {
                current.messageHandler.error(e,"Error verifying trust chain");
            }
        }
        return false;
    }


    /** This method extracts the AuthzDomainID from the certificate
     * @param current The current device
     * @param certificate The certificate where the information is going to be extracted
     * @return The AuthzDomoinID or a null in case of error
     */
    public static String getAuthzDomainIDFromIssuerAlternativeName(PTascNode current,X509Certificate certificate){
        try {
            if (certificate.getIssuerAlternativeNames()!=null) {
                for (List<?> issuerAlternativeName : certificate.getIssuerAlternativeNames()) {
                    int alternateNameType = (Integer) issuerAlternativeName.get(0);
                    if (alternateNameType == GeneralName.rfc822Name) {
                        return (String) issuerAlternativeName.get(1);
                    }
                }
            }
        } catch (CertificateParsingException e) {
            current.messageHandler.error(e,"Error getting the AuthzID from the sender certificate");
        }
        return null;
    }


    /** This method contacts the Authzforce server PAP and attempts to generate a new domain on it
     * @param current The current device
     * @param domainName The name of the domain to be generated
     * @param authzURL The Authzforce server URL
     * @return The generated domain id or null in case of error
     */
    public static String createDomainInAuthzforce(PTascNode current,String domainName,String authzURL){
        try {

            if (isXMLInputUnSafe(domainName)) throw new IOException("Bad input");

            OkHttpClient okHttpClient = new OkHttpClient();

            String createDomainXML = new String(ByteStreams.toByteArray(Objects.requireNonNull(Utils.class.getClassLoader().getResourceAsStream("createDomain.xml")))).replaceAll("-\\.ID", domainName);
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/xml"), createDomainXML);
            Request createDomainRequest = new Request.Builder()
                    .url(authzURL + "/domains")
                    .post(requestBody)
                    .build();
            Response response = okHttpClient.newCall(createDomainRequest).execute();
            String responseString = response.body().string();

            //If the response has a success status code the domain was successfully generated
            //Otherwise throw error and exit

            response.body().close();

            if (response.code() >= 200 && response.code() < 300) {
                return responseString.split("title=")[1].split("\"")[1];
            }
        } catch (IOException e) {
            current.messageHandler.error(e,"Error creating domain in Authzforce server");
        }
        return null;
    }

    /** Verifies if the input does not contain any XML data<br>
     * This is used by the methods that interact with the Authzforce server to ensure that the access control is done right
     * @param toCheck The input to be checked
     * @return A boolean stating if the input is safe or not
     */
    public static boolean isXMLInputUnSafe(String toCheck) {
        if (toCheck == null || ("".equals(toCheck))) return true;
        for(char c : toCheck.toCharArray()){
            if (!((c >= 'a' && c<='z') || (c>='A' && c<='Z') || (c>='0' && c<='9') || c=='*' || c=='-' || c=='.' || c=='&' || c=='_'|| c=='/' || c=='=' || c== '?' || c==':')) return true;
        }
        return false;
    }


    /** This method connects to the Authzforce PAP server and attempts to add a policy set to a domain
     * @param current The current device
     * @param domain The pTASC pool where the manager resides
     * @param authzDomainId The Authzforce domain that will receive the policy set
     * @param authzURL The Authzforce server URL
     * @return A boolean stating if the policy set was added successfully
     */
    public static boolean addPolicy(PTascNode current,String domain,String authzDomainId,String authzURL){
        try {

            if (isXMLInputUnSafe(domain)) return false;

            OkHttpClient okHttpClient = new OkHttpClient();
            InputStream policyReader = Utils.class.getClassLoader().getResourceAsStream("createPolicy.xml");
            String policyTemplate = new String(ByteStreams.toByteArray(Objects.requireNonNull(policyReader)));
            policyTemplate = policyTemplate.replaceAll("-\\.PolicySetID", domain);
            policyTemplate = policyTemplate.replaceAll("-\\.Description", domain + " policies");
            String policySet = policyTemplate.replaceAll("-\\.Domain", domain);
            RequestBody policyAdditionRequestBody = RequestBody.create(MediaType.parse("application/xml"), policySet);
            Request policyAdditionRequest = new Request.Builder()
                    .url(authzURL + "/domains/" + authzDomainId + "/pap/policies")
                    .post(policyAdditionRequestBody)
                    .build();
            Response policyAdditionResponse = okHttpClient.newCall(policyAdditionRequest).execute();

            policyAdditionResponse.body().close();

            //If the response has a success status code the domain was successfully generated
            //Otherwise throw error and exit
            return (policyAdditionResponse.code() >= 200 && policyAdditionResponse.code() < 300);
        } catch (IOException e) {
            current.messageHandler.error(e,"Error adding domain policy to the authzforce server");
        }
        return false;
    }

    /** This method activates the generated policy set on the domain specified on the argument
     * @param current  The current device
     * @param domainName The pTASC pool where the manager resides
     * @param authzDomainId The Authzforce domain that will receive the policy set
     * @param authzURL The Authzforce server URL
     * @return A boolean stating if the policy set was activated successfully
     */
    public static boolean activatePolicy(PTascNode current,String domainName,String authzDomainId,String authzURL){
        try {

            if (isXMLInputUnSafe(domainName)) return false;

            OkHttpClient okHttpClient = new OkHttpClient();
            String activationXMLTemplate = new String(ByteStreams.toByteArray(Objects.requireNonNull(Utils.class.getClassLoader().getResourceAsStream("activatePolicy.xml"))));
            activationXMLTemplate = activationXMLTemplate.replaceAll("-\\.PolicyID",domainName);

            RequestBody activationRequestBody = RequestBody.create(MediaType.parse("application/xml"),activationXMLTemplate);

            Request request = new Request.Builder()
                    .url(authzURL+"/domains/"+authzDomainId+"/pap/pdp.properties")
                    .put(activationRequestBody)
                    .build();

            Response response = okHttpClient.newCall(request).execute();

            response.body().close();
            return (response.code()>=200 && response.code()<=300);
        } catch (IOException e) {
            current.messageHandler.error(e,"Error activating policy for the domain");
        }
        return false;
    }

    /** This method reaches the AuthzForce PDP server and attempts to do an authorization check, returning the authorization result
     * @param current The current device
     * @param authzDomainId The requester Authzforce domain extracted from the certificate
     * @param authzURL The Authzforce server URL
     * @param requesterUSSN The requester USSN
     * @param URL The URL that the requester is trying to reach
     * @param entityID The FIWARE Orion entity ID that the requester is trying to reach
     * @param method The HTTP method that the requester is trying to execute on FIWARE ORion
     * @return A boolean stating if the request is allowed (true) or denied (false)
     */
    public static boolean requestPermission(PTascNode current,String authzDomainId,String authzURL,String requesterUSSN,String URL,String entityID,String method){
        try {

            if (isXMLInputUnSafe(requesterUSSN)) return false;
            if (isXMLInputUnSafe(URL)) return false;
            if (isXMLInputUnSafe(entityID)) return false;

            System.out.println("ALLOWED");

            String requestPermissionTemplateXML = new String(ByteStreams.toByteArray(Objects.requireNonNull(Utils.class.getClassLoader().getResourceAsStream("requestPermission.xml"))));
            requestPermissionTemplateXML = requestPermissionTemplateXML.replaceAll("-\\.USSN",requesterUSSN);
            requestPermissionTemplateXML = requestPermissionTemplateXML.replaceAll("-\\.URL",URL);
            requestPermissionTemplateXML = requestPermissionTemplateXML.replaceAll("-\\.ENTITY_ID",entityID);
            requestPermissionTemplateXML = requestPermissionTemplateXML.replaceAll("-\\.METHOD",method);

            RequestBody requestBody = RequestBody.create(MediaType.parse("application/xml"),requestPermissionTemplateXML);


            Request request = new Request.Builder()
                    .url(authzURL+"/domains/"+authzDomainId+"/pdp")
                    .post(requestBody)
                    .build();

            OkHttpClient okHttpClient = new OkHttpClient();

            Response response = okHttpClient.newCall(request).execute();

            if(response.code()>=200 && response.code()<=300){
                boolean permit = response.body().string().split("Decision")[1].contains("Permit");
                response.body().close();
                return permit;
            }

        } catch (IOException e) {
            current.messageHandler.error(e,"Error requesting access permission on authzforce server");
        }
        return false;
    }


    /** This method generates a QR code containing the text argument as content
     * @param current The current device
     * @param text The string to be encoded
     * @param file The file where the QR code will be stored
     */
    public static void writeQrToFile(PTascNode current, String text, File file) {
        try {
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, 300, 300);
            try {
                if (file != null) {
                    FileOutputStream fos = new FileOutputStream(file);
                    MatrixToImageWriter.writeToStream(bitMatrix, "png", fos);
                    fos.flush();
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (WriterException e) {
            current.messageHandler.error(e,"Error generating QR code");
        }
    }


}