package inconnu.ptasc;

import inconnu.ptasc.communication.Communication;
import inconnu.ptasc.communication.processors.PTASCCommunicationProcessor;
import org.apache.commons.lang3.tuple.Pair;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.math.ec.ECPoint;
import java.io.File;
import java.io.IOException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.Semaphore;

/**
 * This is the base for all the devices on pTASC
 */
public class PTascNode {
    public List<X509Certificate> trustAnchors;
    public X509Certificate[] myCertificate;
    public PrivateKey ECDSAprivKey;
    public PublicKey ECDSApubKey;
    public ECPublicKeyParameters ECIESpubKey;
    public ECPrivateKeyParameters ECIESprivKey;
    public ECPoint G;
    public String name;
    public String pool;
    public String authzURL;
    public Semaphore certificateUpdateSemaphore;
    public MessageHandler messageHandler;
    public List<Thread> serviceThreads;
    public ScheduledFuture<?> scheduledFuture;
    public String crlIp;
    public boolean needsToContactExternalCRL;
    public String endpointUSSN;
    List<PTASCCommunicationProcessor> processors;
    Semaphore processorSemaphore;


    /** This instantiates the super class PTascNode on which clients and managers are based upon
     * @param messageHandler The message handler to be used on the node being instantiated
     */
    public PTascNode(MessageHandler messageHandler){
        serviceThreads = new LinkedList<>();
        this.messageHandler=messageHandler;
        Security.addProvider(new BouncyCastleProvider());
        certificateUpdateSemaphore = new Semaphore(1);
        processors = new LinkedList<>();
        processorSemaphore = new Semaphore(1);
    }


    /** This method opens the communication channel and sends the message and the selected processor name, returning the communication output in byte[]
     * @param destinationUSSN The destination DNS name
     * @param destinationPORT The destination receiving port
     * @param destinationProcessor The processor name
     * @param message The message to be sent in bytes
     * @return A byte[] containing the communication output or null on case of error
     */
    public byte[] communicate(String destinationUSSN, int destinationPORT, String destinationProcessor, byte[] message){
        try {
            return Communication.communicate(destinationUSSN, destinationPORT, destinationProcessor, message, this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /** This method starts the provisioning process of a PTascNode into a manager pool, the pool being the poolToRegister argument
     * @param ManagerECIESpubKey The ECIES public key of the manager of the pool where the device wants to be provisioned into
     * @param ManagerECDSApubKey The ECDSA public key of the manager of the pool where the device wants to be provisioned into
     * @param myUSSN The DNS name of the current device including the pool name
     * @param poolToRegister The pool where the device wants to be provisioned into
     * @param yubikeyOTP A OTP from the a yubikey that the pool manager has registered
     * @param writeCertificateTo The file where the certificate resulted from the provision will be stored
     * @return Return a pair containing the certificate and the certificate chain resulted from a successful provisioning
     */
    public Pair<X509Certificate, List<X509Certificate>> provision(ECPublicKeyParameters ManagerECIESpubKey, PublicKey ManagerECDSApubKey, String myUSSN, String poolToRegister, String yubikeyOTP, File writeCertificateTo){
        try {
            certificateUpdateSemaphore.acquire();
            byte[] salt = new byte[32];
            new SecureRandom().nextBytes(salt);
            Pair<X509Certificate, List<X509Certificate>> x509CertificateListPair = Provisioning.provisionRequest(this, ManagerECIESpubKey, ManagerECDSApubKey, salt, myUSSN, poolToRegister, yubikeyOTP, writeCertificateTo);
            certificateUpdateSemaphore.release();
            return x509CertificateListPair;
        } catch (InterruptedException e) {
            this.messageHandler.error(e,"Provisioning: Unable to get semaphore");
        }
        return null;
    }

    /**
     * Stops the device services initiated
     */
    public void stopServices(){
        if (scheduledFuture!=null) scheduledFuture.cancel(true);
        for (Thread t : serviceThreads){
            t.interrupt();
        }
    }

    /** Sets the Authzforce URL
     * @param authzURL The Authzforce URL
     */
    public void setAuthzURL(String authzURL){
        this.authzURL = authzURL;
    }

    /** This method adds a communication processor to the list of existing communication processors<br>
     * The selection of the processor is based on the processor name <br>
     * The addition of processors and the access to the processor list is synchronized using semaphores
     * @param processor The processor to be added
     * @return A boolean stating if the communication processor was added or not
     */
    public boolean addCommunicationProcessor(PTASCCommunicationProcessor processor){
        boolean value = false;
        try {
            processorSemaphore.acquire();
            value = processors.add(processor);
            processorSemaphore.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return value;
    }

    /**Gets a list of processors, blocking the access while an addition is going on
     * @return The list of processors
     */
    public List<PTASCCommunicationProcessor> getProcessors(){
        List<PTASCCommunicationProcessor> p = null;
        try {
            processorSemaphore.acquire();
            p = processors;
            processorSemaphore.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return p;
    }
}