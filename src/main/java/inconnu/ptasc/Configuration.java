package inconnu.ptasc;

import java.io.File;

public class Configuration {
    public static int MANAGER_PORT =8442;
    public static int DEVICE_COMMUNICATION_PORT=5001;
    public static int MAX_MESSAGE_LENGTH=10*1024*1024; //MAX PAYLOAD ON MESSAGES RECEIVED and SENT via PTASC
    public static int UPLOADDOWNLOAD_CHUNK_LENGTH=5*1024*1024;
    public static final String PTASC_LOG="====PTASC LOG==== ";
    public static File TEMP_PATH = new File("/tmp/");
}
